import React from 'react';

import OrderList from './app/orders/Container';
import { SingleOrder, CreateOrder, EditOrder } from './app/orders/screens';
import { HomeContainer } from './app/home';
import { IconComponent } from './app/common';
import ProductScreen,
  { 
  AddProductScreen,
  SingleProductScreen,
  EditProductContainer,
} from './app/products';
import ProductTypesScreen, { AddProductTypeScreen, CategoryDetailsScreen } from './app/productTypes';
import SalesScreen, { SaleDetailsScreen, AddSaleScreen } from './app/sales';

import {
  createBottomTabNavigator,
  createStackNavigator
 } from 'react-navigation';

const ProductsStack = createStackNavigator(
  {
    Prices: ProductScreen,
    AddProduct: AddProductScreen,
    SingleProduct: SingleProductScreen,
    EditProduct: EditProductContainer,
    ProductTypes: ProductTypesScreen,
    AddProductType: AddProductTypeScreen,
  },
  {
    initialRouteName: 'Prices',
  }
);

// PricelistStack.navigationOptions = ({ navigation }) => {
//   let tabBarVisible = true;
//   if (navigation.state.index > 0) {
//     tabBarVisible = false;
//   }

//   return {
//     tabBarVisible,
//   }; 
// };

const ProductTypesStack = createStackNavigator(
  {
    ProductTypes: ProductTypesScreen,
    AddProductType: AddProductTypeScreen,
    CategoryDetails: CategoryDetailsScreen
  },
  {
    initialRouteName: 'ProductTypes',
  }
);

const SalesStack = createStackNavigator(
  {
    Sales: SalesScreen,
    AddSale: AddSaleScreen,
    SaleDetails: SaleDetailsScreen,
  },
  {
    initialRouteName: 'Sales',
  }
);

const OrdersStack = createStackNavigator(
  {
    AddOrder: CreateOrder,
    ViewOrders: OrderList,
    SingleOrder: SingleOrder,
    EditOrder: EditOrder,
  },
  {
    initialRouteName: 'ViewOrders',
  }
);

const TabNavigator = createBottomTabNavigator(
  {
    Products: ProductsStack,
    Home: HomeContainer,
    Orders: OrdersStack,
    Categories: ProductTypesStack,
    Sales: SalesStack,
  },
  {
    order: ['Home', 'Orders', 'Sales', 'Products', 'Categories'],
    navigationOptions: ({ navigation }) => ({
      tabBarIcon: ({ horizontal, tintColor }) => {
        const { routeName } = navigation.state;
        let iconName;
        if (routeName === 'Home') {
          iconName = 'home';
        } else if (routeName === 'Sales') {
          iconName = 'attach-money';
        } else if (routeName === 'Products') {
          iconName = 'loyalty';
        } else if (routeName === 'Orders') {
          iconName = 'domain';
        } else if (routeName === 'Categories') {
          iconName = 'view-list';
        }
        return <IconComponent name={iconName} size={horizontal ? 25.8: 30.8 } color={tintColor} />
      },
    }),
    tabBarOptions: {
      activeTintColor: '#FF9900',
      inactiveTintColor: '#442C2E',
    }
  }
);

export default App = () => {
  return (
    <TabNavigator />
);
}
