import gql from "graphql-tag";

export const DELIVERY_FRAGMENT = gql`
  fragment Delivery on Delivery {
    id
    type
    location
    cost
    time
  }
`;
