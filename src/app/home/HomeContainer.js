import React from 'react';
import { View, StyleSheet } from 'react-native';
import { CardComponent, TextComponent } from '../common';

export const HomeContainer = (props) => {
  const { navigation: { navigate } } = props;
  return (
    <View style={styles.container}>
      <View>
        <TextComponent text="Crafted pizza" style={styles.title} />
        <TextComponent text="Your take-away pizza shop" style={styles.subTitle} />
      </View>
      <View style={styles.cardContainer}>
        <CardComponent style={styles.card} onPress={() => navigate('Orders')}>
          <TextComponent
            text='Orders'
            style={styles.cardText}
          />
        </CardComponent>
        <CardComponent style={styles.card} onPress={() => navigate('Sales')}>
          <TextComponent
            text='Sales'
            style={styles.cardText}
          />
        </CardComponent>
      </View>
      <View style={styles.cardContainer}>
        <CardComponent style={styles.card} onPress={() => navigate('Products')}>
          <TextComponent
            text='Products'
            style={styles.cardText}
          />
        </CardComponent>
        <CardComponent style={styles.card} onPress={() => navigate('Categories')}>
          <TextComponent
            text='Categories'
            style={styles.cardText}
            />
        </CardComponent>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'space-around',
    backgroundColor: '#FFF5EE'
  },
  title: {
    textAlign: 'center',
    fontSize: 28,
    color: '#442C2E',
    marginBottom: 8
  },
  subTitle:{
    fontSize: 18,
    color: '#B9B9B9',
    textAlign: 'center',
  },
  cardContainer: {
    flexDirection: 'row',
    justifyContent: "space-around"
  },
  card: {
    width: 150,
    height: 150,
    padding: 20,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#EF4DB6',
    borderColor: 'rgba(0, 0 ,0 ,0.65)',
    borderRadius: 27,
    shadowColor: '#B9B9B9',
    shadowRadius: 3
  },
  cardText: {
    color: 'white',
    fontSize: 22
  }
});
