import { get, post, patch, deleteRequest } from '../../../../services/api';
import actions from './actions';

const {
  getAllPizzaTypesRequest,
  getAllPizzaTypesSuccess,
  getAllPizzaTypesError,
  addPizzaTypeRequest,
  addPizzaTypeSuccess,
  addPizzaTypeError,
  editPizzaTypeRequest,
  editPizzaTypeSuccess,
  editPizzaTypeError,
  deletePizzaTypeRequest,
  deletePizzaTypeSuccess,
  deletePizzaTypeError
 } = actions; 
export const getAllPizzaTypes = () => (
  async dispatch => {
    try {
      dispatch(getAllPizzaTypesRequest());
      const pizzaprices = await get('product?type=pizza');
      dispatch(getAllPizzaTypesSuccess(pizzaprices));
    } catch (error) {
      dispatch(getAllPizzaTypesError(error))
    }
  }
);

export const addPizzaType = (product) => (
  async dispatch => {
    try {
      dispatch(addPizzaTypeRequest());
      const pizzaprice = await post('product', product);
      dispatch(addPizzaTypeSuccess(pizzaprice));
    } catch (error) {
      dispatch(addPizzaTypeError());
    }
  }
);

export const editPizzaType = (pizzaType) => (
  async dispatch => {
    try {
      dispatch(editPizzaTypeRequest());
      const editedPizzaType = await patch(`product/${pizzaType.id}`, pizzaType);
      dispatch(editPizzaTypeSuccess(editedPizzaType));
    } catch (error) {
      dispatch(editPizzaTypeError());
    }
  }
);

export const deletePizzaType = (id) => (
  async dispatch => {
    try {
      dispatch(deletePizzaTypeRequest());
      const deletedPizzaType = await deleteRequest(`product/${id}`);
      dispatch(deletePizzaTypeSuccess(deletedPizzaType));
    } catch (error) {
      dispatch(deletePizzaTypeError());
    }
  }
);
