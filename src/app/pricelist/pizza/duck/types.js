const GET_ALL_PIZZA_TYPES_REQUEST = 'GET_ALL_PIZZA_TYPES_REQUEST';
const GET_ALL_PIZZA_TYPES_SUCCESS = 'GET_ALL_PIZZA_TYPES_SUCCESS';
const GET_ALL_PIZZA_TYPES_ERROR = 'GET_ALL_PIZZA_TYPES_ERROR';
const ADD_PIZZA_TYPE_REQUEST = 'ADD_PIZZA_TYPE_REQUEST';
const ADD_PIZZA_TYPE_SUCCESS = 'ADD_PIZZA_TYPE_SUCCESS';
const ADD_PIZZA_TYPE_ERROR = 'ADD_PIZZA_TYPE_ERROR';
const EDIT_PIZZA_TYPE_REQUEST = 'EDIT_PIZZA_TYPE_REQUEST';
const EDIT_PIZZA_TYPE_SUCCESS = 'EDIT_PIZZA_TYPE_SUCCESS';
const EDIT_PIZZA_TYPE_ERROR = 'EDIT_PIZZA_TYPE_ERROR';
const DELETE_PIZZA_TYPE_REQUEST = 'DELETE_PIZZA_TYPE_REQUEST';
const DELETE_PIZZA_TYPE_SUCCESS = 'DELETE_PIZZA_TYPE_SUCCESS';
const DELETE_PIZZA_TYPE_ERROR = 'DELETE_PIZZA_TYPE_ERROR';

export default {
  GET_ALL_PIZZA_TYPES_ERROR,
  GET_ALL_PIZZA_TYPES_SUCCESS,
  GET_ALL_PIZZA_TYPES_REQUEST,
  ADD_PIZZA_TYPE_ERROR,
  ADD_PIZZA_TYPE_SUCCESS,
  ADD_PIZZA_TYPE_REQUEST,
  EDIT_PIZZA_TYPE_ERROR,
  EDIT_PIZZA_TYPE_REQUEST,
  EDIT_PIZZA_TYPE_SUCCESS,
  DELETE_PIZZA_TYPE_ERROR,
  DELETE_PIZZA_TYPE_REQUEST,
  DELETE_PIZZA_TYPE_SUCCESS
};
