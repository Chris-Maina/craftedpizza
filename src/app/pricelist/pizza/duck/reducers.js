import types from './types';
import initialState from '../../../../reducers/initialState';
import editItemInArray from '../../../../services/editItemInArray'
import deleteItemInArray from '../../../../services/deleteItemInArray';

export const pizzaList = (state=initialState.priceLists.pizzaList, {type, payload}) => {
  switch (type) {
    case types.GET_ALL_PIZZA_TYPES_REQUEST: 
      return {
        ...state,
        loading: true
      }
    case types.GET_ALL_PIZZA_TYPES_SUCCESS:
      return {
        ...state,
        pizzas: payload,
        loading: false,
        hasError: false
      }
    case types.GET_ALL_PIZZA_TYPES_ERROR: 
      return {
        ...state,
        hasError: true,
        loading: false
      }
    case types.ADD_PIZZA_TYPE_REQUEST: 
      return {
        ...state,
        loading: true
      }
    case types.ADD_PIZZA_TYPE_SUCCESS: 
      return {
        ...state,
        pizzas: [...payload, ...state.pizzas],
        loading: false,
        hasError: false
      }
    case types.ADD_PIZZA_TYPE_ERROR: 
      return {
        ...state,
        hasError: true,
        loading: false
      }
    case types.EDIT_PIZZA_TYPE_REQUEST:
      return {
        ...state,
        loading: true
      }
    case types.EDIT_PIZZA_TYPE_SUCCESS:
      newPizzas = editItemInArray(payload, state.pizzas);
      return {
        ...state,
        loading: false,
        pizzas: newPizzas,
        hasError: false
      }
    case types.EDIT_PIZZA_TYPE_ERROR: 
      return {
        ...state,
        hasError: true,
        loading: false
      }
    case types.DELETE_PIZZA_TYPE_REQUEST:
      return {
        ...state,
        loading: true,
      }
    case types.DELETE_PIZZA_TYPE_SUCCESS:
      return {
        ...state,
        loading: true,
        pizzas: deleteItemInArray(payload, state.pizzas),
        hasError: false
      }
    case types.DELETE_PIZZA_TYPE_ERROR:
      return {
        ...state,
        loading: false,
        hasError: true
      }
    default:
      return state;
  }
}
