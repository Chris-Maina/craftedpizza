import types from './types';

const getAllPizzaTypesRequest = () => ({
  type: types.GET_ALL_PIZZA_TYPES_REQUEST
});

const getAllPizzaTypesSuccess = (pizzaprices) => ({
  type: types.GET_ALL_PIZZA_TYPES_SUCCESS,
  payload: pizzaprices
});

const getAllPizzaTypesError = () => ({
  type: types.GET_ALL_PIZZA_TYPES_ERROR,
});

const addPizzaTypeRequest = () => ({
  type: types.ADD_PIZZA_TYPE_REQUEST,
});

const addPizzaTypeSuccess = (pizzaprice) => ({
  type: types.ADD_PIZZA_TYPE_REQUEST,
  payload: pizzaprice,
});

const addPizzaTypeError = () => ({
  type: types.ADD_PIZZA_TYPE_REQUEST
});

const editPizzaTypeRequest = () => ({
  type: types.EDIT_PIZZA_TYPE_REQUEST
});

const editPizzaTypeSuccess = (pizzaType) => ({
  type: types.EDIT_PIZZA_TYPE_SUCCESS,
  payload: pizzaType
});

const editPizzaTypeError = () => ({
  type: types.EDIT_PIZZA_TYPE_ERROR
});

const deletePizzaTypeRequest = () => ({
  type: types.DELETE_PIZZA_TYPE_REQUEST
});

const deletePizzaTypeSuccess = (pizzaType) => ({
  type: types.DELETE_PIZZA_TYPE_SUCCESS,
  payload: pizzaType
});

const deletePizzaTypeError = () => ({
  type: types.DELETE_PIZZA_TYPE_ERROR
});

export default {
  getAllPizzaTypesRequest,
  getAllPizzaTypesSuccess,
  getAllPizzaTypesError,
  addPizzaTypeRequest,
  addPizzaTypeSuccess,
  addPizzaTypeError,
  editPizzaTypeRequest,
  editPizzaTypeSuccess,
  editPizzaTypeError,
  deletePizzaTypeError,
  deletePizzaTypeRequest,
  deletePizzaTypeSuccess
};
