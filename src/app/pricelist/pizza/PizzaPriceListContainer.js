import React, { Component } from 'react';
import { connect } from 'react-redux';
import { ActivityIndicator } from 'react-native';
import { getAllPizzaTypes } from './duck';
import {
  ItemListContainer
} from '../ItemListContainer';
import { ErrorComponent } from '../../common';

class _PizzaPriceListContainer extends Component{
  static navigationOptions = {
    title: 'Pizza',
  };

  componentDidMount() {
    this.props.getAllPizzaTypes();
  }

  handleAddClick = () => {
    const { navigation: { navigate } } = this.props;
    navigate('Add', { priceListType: 'pizza' });
  }

  handleItemClick = (item) => {
    const { navigation: { navigate } } = this.props;
    navigate('ProductDetails', { item, title: 'Pizza' } );
  } 

  render() {
    const { pizzas, loading, hasError, getAllPizzaTypes } = this.props;
    let pizzaPriceListHtml;
    if (loading) {
      pizzaPriceListHtml = (<ActivityIndicator size='large' color='#EF4DB6' />);
    } 
    else if (!loading && hasError) {
      pizzaPriceListHtml = (<ErrorComponent message='There was an error loading pizza prizes' retry={getAllPizzaTypes} />)
    }
    else {
      pizzaPriceListHtml = (
      <ItemListContainer
        items={pizzas}
        handleAddClick={this.handleAddClick}
        handleItemClick={this.handleItemClick}
      />
      );
    }
    return pizzaPriceListHtml;
  }
}

const mapActionsToProps = dispatch => ({
  getAllPizzaTypes() {
    return dispatch(getAllPizzaTypes());
  }
});

const mapStateToProps = ({pizzaList}) => {
  return {
  pizzas: pizzaList.pizzas,
  loading: pizzaList.loading,
  hasError: pizzaList.hasError 
  }
};

export const PizzaPriceListContainer = connect(
  mapStateToProps,
  mapActionsToProps
  )(_PizzaPriceListContainer);
