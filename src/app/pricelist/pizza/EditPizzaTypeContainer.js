import React, { Component } from 'react';
import { connect } from 'react-redux';
import { FormComponent } from '../../common/FormComponent';
import validateFormFields from '../../../services/formValidator';
import { editPizzaType } from './duck';

export class _EditPizzaTypeContainer extends Component {
  static navigationOptions = {
    title: 'Edit',
  };

  constructor(props) {
    super(props);
    const { item } = props.navigation.state.params;
    this.state = {
      title: item.size,
      errors: {},
      product: {
        name: item.name,
        size: item.size,
        price: item.price,
        id: item.id
      },
      sizes: [
        {
          id: 0,
          title: 'Large',
          selected: false,
          key: 'sizes'
        },
        {
          id: 1,
          title: 'Medium',
          selected: false,
          key: 'sizes'
        },
        {
          id: 2,
          title: 'Small',
          selected: false,
          key: 'sizes'
        },
      ],
    };
  }
  renderValidationError = field => {
    if (typeof this.state.errors[field] !== "undefined") {
      return this.state.errors[field];
    }
    return "";
  };

  toggleSelectedSize = (id, key) => {
    const { product } = this.state;
    let tempSizes = this.state[key];
    tempSizes[id].selected = !tempSizes[id].selected;
    product['size'] = tempSizes[id].title
    this.setState({ [key]: tempSizes, title: tempSizes[id].title, product });
  }

  handleNameChange = (name) => {
    const { product } = this.state;
    product['name'] = name;
    this.setState({ product });
  }

  handlePriceChange = (price) => {
    const { product } = this.state;
    product['price'] = price;
    this.setState({ product });
  }

  handleSubmit = () => {
    const { product } = this.state;
    const { navigation, editPizzaType } = this.props;
    const errors = validateFormFields(product);
    if (Object.keys(errors).length === 0) {
      editPizzaType(product);
      navigation.push('PizzaPrices');
    } else {
      this.setState({ errors });
    }
  }

  render () {
    const { title, sizes, product } = this.state;
    const fields = [
      {
        name: 'Name',
        type: 'input',
        placeHolder: 'Enter product name',
        value: product.name,
        cb: this.handleNameChange,
        renderValidationError: this.renderValidationError
      },
      {
        name: 'Sizes',
        type: 'dropdown',
        cb: this.toggleSelectedSize,
        options: sizes,
        renderValidationError: this.renderValidationError
      },
      {
        name: 'Price',
        type: 'input',
        placeHolder: 'Price',
        value: product.price,
        cb: this.handlePriceChange,
        renderValidationError: this.renderValidationError
      }
    ];

    return (
      <FormComponent
        fields={fields}
        iconName='check'
        title={title}
        handleSubmit={this.handleSubmit}
      />
    );
  }
}

const mapActionsToProps = (dispatch) => ({
  editPizzaType(item) {
    return dispatch(editPizzaType(item));
  }
});

export const EditPizzaTypeContainer = connect(null, mapActionsToProps)(_EditPizzaTypeContainer);
