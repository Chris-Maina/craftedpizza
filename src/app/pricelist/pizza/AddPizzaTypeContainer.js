import React, { Component } from 'react';
import { connect } from 'react-redux';
import {
  FormComponent
 } from '../../common';
 import { addPizzaType } from './duck';
 import validateFormFields from '../../../services/formValidator';

class _AddPizzaTypeContainer extends Component {
  static navigationOptions = {
    title: 'Add',
  };
  constructor() {
    super();
    this.state = {
      title: 'Sizes',
      sizes: [
        {
          id: 0,
          title: 'Large',
          selected: false,
          key: 'sizes'
        },
        {
          id: 1,
          title: 'Medium',
          selected: false,
          key: 'sizes'
        },
        {
          id: 2,
          title: 'Small',
          selected: false,
          key: 'sizes'
        },
      ],
      product: {
        name: '',
        size: '',
        price: '',
        type: 'pizza'
      },
      errors: {}
    };
  }

  toggleSelectedSize = (id, key) => {
    const { product } = this.state;
    let tempSizes = this.state[key];
    tempSizes[id].selected = !tempSizes[id].selected;
    product['size'] = tempSizes[id].title
    this.setState({ [key]: tempSizes, title: tempSizes[id].title, product });
  }

  handleProductNameChange = (text) => {
    const { product } = this.state;
    product['name'] = text;
    this.setState({ product });
  }

  handlePriceChange = (text) => {
    const { product } = this.state;
    product['price'] = text;
    this.setState({ product });
  }

  handleSubmit = () => {
    const { product } = this.state;
    const { addPizzaType, navigation } = this.props;
    const errors = validateFormFields(product);
    if (Object.keys(errors).length === 0) {
      addPizzaType(product);
      navigation.push('PizzaPrices');
    } else {
      this.setState({ errors });
    }
  }

  renderValidationError = (field) => {
    if (typeof this.state.errors[field] !== 'undefined') {
      return this.state.errors[field];
    }
    return '';
  }

  render() {
    const { title, product, sizes } = this.state;
    const fields = [
      {
        name: 'Name',
        type: 'input',
        placeholder: 'Name',
        value: product.name,
        cb: this.handleProductNameChange,
        renderValidationError: this.renderValidationError
      },
      {
        name: 'Sizes',
        type: 'dropdown',
        options: sizes,
        cb: this.toggleSelectedSize,
        renderValidationError: this.renderValidationError
      },
      {
        name: 'Price',
        type: 'input',
        placeholder: 'Price',
        value: product.price,
        cb: this.handlePriceChange,
        renderValidationError: this.renderValidationError
      }    
    ];
    return(
      <FormComponent
        fields={fields}
        iconName='check'
        title={title}
        handleSubmit={this.handleSubmit}
      />
    );
  }
}

const mapActionsToProp = (dispatch) => ({
  addPizzaType(product) {
    return dispatch(addPizzaType(product))
  }
})

export const AddPizzaTypeContainer = connect(
  null,
  mapActionsToProp,
)(_AddPizzaTypeContainer)
