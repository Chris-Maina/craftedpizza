import React, { Component } from 'react';
import { connect } from 'react-redux';
import { FormComponent } from '../../common/FormComponent';
import validateFormFields from '../../../services/formValidator';
import { editFries } from './duck';

export class _EditFriesTypeContainer extends Component {
  static navigationOptions = {
    title: 'Edit',
  };

  constructor(props) {
    super(props);
    const { item } = props.navigation.state.params;
    this.state = {
      errors: {},
      product: {
        name: item.name,
        price: item.price,
        id: item.id
      },
    };
  }

  renderValidationError = field => {
    if (typeof this.state.errors[field] !== "undefined") {
      return this.state.errors[field];
    }
    return "";
  };

  handleNameChange = (name) => {
    const { product } = this.state;
    product['name'] = name;
    this.setState({ product });
  }

  handlePriceChange = (price) => {
    const { product } = this.state;
    product['price'] = price;
    this.setState({ product });
  }

  handleSubmit = () => {
    const { product } = this.state;
    const { navigation, editFries } = this.props;
    const errors = validateFormFields(product);
    if (Object.keys(errors).length === 0) {
      editFries(product);
      navigation.push('FriesPrices');
    } else {
      this.setState({ errors });
    }
  }

  render () {
    const { product: { name, price } } = this.state;
    const fields = [
      {
        name: 'Name',
        type: 'input',
        placeholder: 'Enter product name',
        value: name,
        cb: this.handleNameChange,
        renderValidationError: this.renderValidationError
      },
      {
        name: 'Price',
        type: 'input',
        placeholder: 'Price',
        value: price,
        cb: this.handlePriceChange,
        renderValidationError: this.renderValidationError
      }
    ];

    return (
      <FormComponent
        fields={fields}
        iconName='check'
        handleSubmit={this.handleSubmit}
      />
    );
  }
}

const mapActionsToProps = dispatch => ({
  editFries(item) {
    return dispatch(editFries(item));
  }
});

export const EditFriesTypeContainer = connect(null, mapActionsToProps)(_EditFriesTypeContainer);
