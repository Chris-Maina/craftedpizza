import React, { Component } from 'react';
import { connect } from 'react-redux';
import { FormComponent } from '../../common';
import validateFormFields from '../../../services/formValidator';
import { addFries } from './duck'

class _AddFriesTypeContainer extends Component {
  static navigationOptions = {
    title: 'Add',
  };

  constructor(props) {
    super(props);
    this.state = {
      errors: {},
      product: {
        name: '',
        price: '',
        type: 'fries'
      }
    }
  }

  renderValidationError = field => {
    if (typeof this.state.errors[field] !== "undefined") {
      return this.state.errors[field];
    }
    return "";
  };

  handleNameChange = name => {
    const { product } = this.state;
    product['name'] = name;
    this.setState({ product });
  }

  handlePriceChange = (price) => {
    const { product } = this.state;
    product['price'] = price;
    this.setState({ product });
  }

  handleSubmit = () => {
    const { product } = this.state;
    const { addFries, navigation } = this.props;
    const errors = validateFormFields(product);
    if (Object.keys(errors).length === 0) {
      addFries(product);
      navigation.push('FriesPrices');
    } else {
      this.setState({ errors });
    }
  }



  render() {
    const { product: { name, price } } = this.state;
    const fields = [
      {
        name: 'Name',
        type: 'input',
        placeholder: 'Enter product name',
        value: name,
        cb: this.handleNameChange,
        renderValidationError: this.renderValidationError
      },
      {
        name: 'Price',
        type: 'input',
        placeholder: 'Price',
        value: price,
        cb: this.handlePriceChange,
        renderValidationError: this.renderValidationError
      }
    ];
    return(
      <FormComponent
        fields={fields}
        iconName='check'
        handleSubmit={this.handleSubmit}
      />
    );
  }
}

const mapActionsToProps = dispatch => ({
  addFries(fries) {
    return dispatch(addFries(fries));
  }
})

export const AddFriesTypeContainer = connect(null, mapActionsToProps)(_AddFriesTypeContainer);
