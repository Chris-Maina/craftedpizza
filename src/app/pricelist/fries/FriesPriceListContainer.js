import React, { Component } from 'react';
import { connect } from 'react-redux';
import { ActivityIndicator } from 'react-native';
import { getAllFriesWithPrices } from './duck'
import { ItemListContainer } from '../ItemListContainer';
import { ErrorComponent } from '../../common';

class _FriesPriceListContainer extends Component{
  static navigationOptions = {
    title: 'Fries',
  };

  componentDidMount() {
    this.props.getAllFriesWithPrices();
  }

  handleAddClick = () => {
    const { navigation: { navigate }} = this.props;
    navigate('Add', { priceListType: 'fries' });
  }

  handleItemClick = (item) => {
    const { navigation: { navigate } } = this.props;
    navigate('ProductDetails', { item, title: 'Fries' } );
  }

  render() {
    const { fries, loading, hasError, getAllFriesWithPrices } = this.props;
    let friesPriceListHtml;
    if(loading) {
      friesPriceListHtml = (<ActivityIndicator size='large' color='#EF4DB6' />);
    } else if (!loading && hasError ) {
      friesPriceListHtml = (<ErrorComponent message='There was an error loading fries prizes' retry={getAllFriesWithPrices} />);
    } else {
      friesPriceListHtml = (
        <ItemListContainer
          items={fries}
          handleAddClick={this.handleAddClick}
          handleItemClick={this.handleItemClick}
        />
      );
    }
    return friesPriceListHtml;
  }
}

const mapStateToProps = ({friesList}) => ({
  fries: friesList.fries,
  loading: friesList.loading,
  hasError: friesList.hasError
});

const mapActionsToProps = dispatch => ({
  getAllFriesWithPrices() {
    return dispatch(getAllFriesWithPrices());
  }
});

export const FriesPriceListContainer = connect(mapStateToProps, mapActionsToProps)(_FriesPriceListContainer);
