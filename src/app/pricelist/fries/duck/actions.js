import types from './types';

const getAllFriesWithPricesRequest = () => ({
  type: types.GET_ALL_FRIES_WITH_PRICES_REQUEST
});

const getAllFriesWithPricesSuccess = (friesWithPrices) => ({
  type: types.GET_ALL_FRIES_WITH_PRICES_SUCCESS,
  payload: friesWithPrices
});

const getAllFriesWithPricesError = () => ({
  type: types.GET_ALL_FRIES_WITH_PRICES_ERROR
});

const addFriesRequest = () => ({
  type: types.ADD_FRIES_REQUEST
});

const addFriesSuccess = (fries) => ({
  type: types.ADD_FRIES_SUCCESS,
  payload: fries
});

const addFriesError = () => ({
  type: types.ADD_FRIES_ERROR
});

const editFriesRequest = () => ({
  type: types.EDIT_FRIES_REQUEST
});

const editFriesSuccess = (fries) => ({
  type: types.EDIT_FRIES_SUCCESS,
  payload: fries
});

const editFriesError = () => ({
  type: types.EDIT_FRIES_ERROR
});

const deleteFriesRequest = () => ({
  type: types.DELETE_FRIES_REQUEST
});

const deleteFriesSuccess = (fries) => ({
  type: types.DELETE_FRIES_SUCCESS,
  payload: fries
});

const deleteFriesError = () => ({
  type: types.DELETE_FRIES_ERROR
});

export default {
  getAllFriesWithPricesError,
  getAllFriesWithPricesRequest,
  getAllFriesWithPricesSuccess,
  addFriesError,
  addFriesRequest,
  addFriesSuccess,
  editFriesRequest,
  editFriesError,
  editFriesSuccess,
  deleteFriesError,
  deleteFriesRequest,
  deleteFriesSuccess
}