import actions from './actions';
import { get, post, patch, deleteRequest } from '../../../../services/api';

export const getAllFriesWithPrices = () => (
  async dispatch => {
    try {
      dispatch(actions.getAllFriesWithPricesRequest());
      const friesWithPrices = await get('product?type=fries');
      dispatch(actions.getAllFriesWithPricesSuccess(friesWithPrices));
    } catch (error) {
      dispatch(actions.getAllFriesWithPricesError());
    }
  }
);

export const addFries = (fries) => (
  async dispatch => {
    try {
      dispatch(actions.addFriesRequest());
      const addedFries = await post('product', fries);
      dispatch(actions.addFriesSuccess(addedFries))
    } catch (error) {
      dispatch(actions.addFriesError());
    }
  }
);

export const editFries = (product) => (
  async dispatch => {
    try {
      dispatch(actions.editFriesRequest());
      const editedFries = await patch(`product/${product.id}`, product);
      dispatch(actions.editFriesSuccess(editedFries));
    } catch (error) {
      dispatch(actions.editFriesError());
    }
  }
);

export const deleteFries = (id) => (
  async dispatch => {
    try {
      dispatch(actions.deleteFriesRequest());
      const deletedFries = await deleteRequest(`product/${id}`);
      dispatch(actions.deleteFriesSuccess(deletedFries));
    } catch (error) {
      dispatch(actions.deleteFriesError());
    }
  }
);
