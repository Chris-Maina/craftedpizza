import initialState from '../../../../reducers/initialState';
import types from './types';
import editItemInArray from '../../../../services/editItemInArray';
import deleteItemInArray from '../../../../services/deleteItemInArray';

export const friesList = (state= initialState.priceLists.friesList, {type, payload}) => {
  switch(type) {
    case types.GET_ALL_FRIES_WITH_PRICES_REQUEST:
      return {
        ...state,
        loading: true,
        hasError: false
      }
    case types.GET_ALL_FRIES_WITH_PRICES_SUCCESS:
      return {
        ...state,
        loading: false,
        fries: payload,
        hasError: false
      }
    case types.GET_ALL_FRIES_WITH_PRICES_ERROR:
      return {
        ...state,
        loading: false,
        hasError: true
      }
    case types.ADD_FRIES_REQUEST:
      return {
        ...state,
        loading: true,
        hasError: false
      }
    case types.ADD_FRIES_SUCCESS:
      return {
        ...state,
        loading: false,
        fries: [...payload, ...state.fries],
        hasError: false
      }
    case types.ADD_FRIES_ERROR:
      return {
        ...state,
        loading: false,
        hasError: true
      }
    case types.EDIT_FRIES_REQUEST:
      return {
        ...state,
        loading: true,
        hasError: false
      }
    case types.EDIT_FRIES_SUCCESS:
      return {
        ...state,
        loading: false,
        fries: editItemInArray(payload, state.fries),
        hasError: false
      }
    case types.EDIT_FRIES_ERROR:
      return {
        ...state,
        loading: false,
        hasError: true
      }
    case types.DELETE_FRIES_REQUEST:
      return {
        ...state,
        loading: true,
        hasError: false
      }
    case types.DELETE_FRIES_SUCCESS:
      return {
        ...state,
        loading: false,
        fries: deleteItemInArray(payload, state.fries),
        hasError: false
      }
    case types.DELETE_FRIES_ERROR:
      return {
        ...state,
        loading: false,
        hasError: true
      }
    default:
      return state
  }
};
