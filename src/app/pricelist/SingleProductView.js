import React, { Component } from 'react';
import { connect } from 'react-redux';
import { StyleSheet, View } from 'react-native';
import {
  ListItemComponent,
  ListContainer,
  TextComponent,
  ButtonComponent,
  IconComponent
 } from '../common';

import { deletePizzaType } from './pizza/duck'
import { deleteFries } from './fries/duck';

export class _SingleProductView extends Component {
  static navigationOptions = {
    title: 'Details',
  };

  handleDeleteClick = (item) => {
    const { navigation: { push }, deletePizzaType, deleteFries } = this.props;
    const { type, id } = item;
    switch(type) {
      case 'pizza':
        deletePizzaType(id);
        push('PizzaPrices');
        break;
      case 'fries':
        deleteFries(id);
        push('FriesPrices');
      default:
        break;
    };
  }

  handleEditClick = () => {
    const { navigation: { navigate, getParam } } = this.props;
    const item = getParam('item');
    switch(item.type) {
      case 'pizza': 
        return navigate('Edit', { item } );
      case 'fries':
        return navigate('EditFries', { item } );
      default:
        return null;
    }  
  }

  renderLabelAndText = (labelText, value) => {
    if(value) return (
      <View style={styles.textContainer}>
        <TextComponent style={styles.labelText} text={labelText}/>
        <TextComponent style={styles.text}  text={value} />
      </View>
    );
  };

  render () {
    const { item, title } = this.props.navigation.state.params;
    return (
      <ListContainer style={styles.container}>
        <ListItemComponent style={styles.card}>
          <View style={styles.headerContainer}>
            <TextComponent style={styles.header} text={title}/>
            <ButtonComponent style={styles.deleteButton} onPress={() => this.handleDeleteClick(item)}>
              <IconComponent
                name='delete'
                size={30}
                color='white'
                style={styles.deleteIcon}
              />
            </ButtonComponent>
          </View>
          <View>
            {
              this.renderLabelAndText('Name', item.name)
            }
            {
              this.renderLabelAndText('Size', item.size)
            }
            {
              this.renderLabelAndText('Price', item.price)
            }
          </View>
        </ListItemComponent>
        <ButtonComponent style={styles.button} onPress={this.handleEditClick}>
          <IconComponent
            name='edit'
            size={40}
            color='white'
            style={styles.editIcon}
          />
        </ButtonComponent>
      </ListContainer>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    position: 'relative'
  },
  card: {
    flexDirection: 'column',
    padding: 20,
    height: 300
  },
  headerContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingBottom: 10,
    borderBottomColor: '#A2A2A2',
    borderBottomWidth: 0.5,
  },
  header: {
    fontSize: 24,
    fontWeight: '400'
  },
  detailsContainer: {
    backgroundColor: 'white'
  },
  textContainer: {
    flexDirection: 'row',
    marginBottom: 22
  },
  labelText: {
    color: '#A2A2A2',
    fontSize: 18,
    textAlign: 'left'
  },
  text: {
    fontSize: 18,
    textAlign: 'left',
    marginLeft: 20
  },
  button: {
    position: 'absolute',
    top: 280,
    right: 20,
    borderRadius: 62.1,
    width: 62.1,
    height: 62.1,
    backgroundColor: '#09A494'
  },
  editIcon: {
    alignSelf: 'center',
    marginTop: 'auto',
    marginBottom: 'auto',
    zIndex: 5,
  },
  deleteIcon: {
    alignSelf: 'center',
    marginTop: 'auto',
    marginBottom: 'auto',
    zIndex: 5,
  },
  deleteButton: {
    borderRadius: 62.1,
    width: 40,
    height: 40,
    backgroundColor: '#BF0F0F'
  }
});

const mapActionsToProps = dispatch => ({
  deletePizzaType(id) {
    return dispatch(deletePizzaType(id));
  },
  deleteFries(id) {
    return dispatch(deleteFries(id));
  }
});

export const SingleProductView = connect(null, mapActionsToProps)(_SingleProductView);
