import React, { Component } from 'react';
import { connect } from 'react-redux'; 
import { FormComponent } from '../common';
import validateFormFields from '../../services/formValidator';
import createField from '../../services/createField';
import { sizes } from '../constants';
import { addFries } from './fries/duck';
import { addPizzaType } from './pizza/duck';
import { addChicken } from './chicken/duck';

class _AddPriceListItemContainer extends Component {
  static navigationOptions = {
    title: 'Add'
  }

  constructor(props) {
    super(props);
    const { priceListType } = this.props.navigation.state.params;
    this.state = {
      priceListType, 
      title: 'Sizes',
      product: {
        name: '',
        price: '',
        size: '',
        type: priceListType
      },
      sizes,
      errors: {}
    }
  }

  /**
   * @name handleNameChange
   * @description on change text handler for name input
   * @param {name} - name of the product
   */
  handleNameChange = name => {
    const { product } = this.state;
    product['name'] = name;
    this.setState({ product });
  }

  /**
   * @name handlePriceChange
   * @description on change text handler for price input
   * @param {price} - price of the product
   */
  handlePriceChange = (price) => {
    const { product } = this.state;
    product['price'] = price;
    this.setState({ product });
  }

  /**
   * @name renderValidationError
   * @description displays error of the field provided if any
   * @param {field} - input field
   */
  renderValidationError = field => {
    if (typeof this.state.errors[field] !== "undefined") {
      return this.state.errors[field];
    }
    return "";
  };

  /**
   * @name handleSubmit
   * @description on submit handler for the product
   * 
   */
  handleSubmit = () => {
    const { product } = this.state;
    const { addFries, addChicken, addPizzaType, navigation } = this.props;
    let errors;
    switch (product.type) {
      case 'fries':
        errors = validateFormFields(product, ['name', 'price']);
        if (Object.keys(errors).length === 0) {
          addFries(product);
          navigation.push('FriesPrices');
        } else {
          this.setState({ errors });
        }
        break;
      case 'chicken':
        errors = validateFormFields(product, ['name', 'price']);
        if (Object.keys(errors).length === 0) {
          addChicken(product);
          navigation.push('ChickenPrices');   
        } else {
          this.setState({ errors });
        }
        break;
      case 'pizza':
        errors = validateFormFields(product, ['name', 'size', 'price']);
        if (Object.keys(errors).length === 0) {
          addPizzaType(product);
          navigation.push('PizzaPrices');
        } else {
          this.setState({ errors });
        }
        break;
      default:
        return null;
    }
  }

  toggleSelectedSize = (id, key) => {
    const { product } = this.state;
    let tempSizes = this.state[key];
    tempSizes[id].selected = !tempSizes[id].selected;
    product['size'] = tempSizes[id].title
    this.setState({ [key]: tempSizes, title: tempSizes[id].title, product });
  }

  cancelClick = () => {
    const { priceListType } = this.props.navigation.state.params;
    this.setState({
      product: {
        name: '',
        price: '',
        size: '',
        type: priceListType
      },
    })
  }

  getFieldsToRender = () => {
    const { product: { name, price, type }, sizes, title } = this.state;
    let fields;
    switch (type) {
      case 'chicken':
        fields = [
          createField('Name', 'input', 'Enter product name', name, null, this.handleNameChange, this.renderValidationError),
          createField('Price', 'input', 'Price per kg', price, null, this.handlePriceChange, this.renderValidationError),
        ];
        return fields;
      case 'fries': 
        fields = [
          createField('Name', 'input', 'Enter product name', name, null, this.handleNameChange, this.renderValidationError),
          createField('Price', 'input', 'Price', price, null, this.handlePriceChange, this.renderValidationError),
        ]
        return fields;
      case 'pizza':
        fields = [
          createField('Name', 'input', 'Name', name, null, this.handleNameChange, this.renderValidationError),
          createField('Size', 'dropdown', 'Name', title, sizes, this.toggleSelectedSize, this.renderValidationError),
          createField('Price', 'input', 'Price', price, null, this.handlePriceChange, this.renderValidationError), 
        ];
        return fields;
      default:
        return fields;
    }
  }

  render () {
    const fields = this.getFieldsToRender();
    return (
      <FormComponent
        fields={fields}
        iconName='check'
        cancelText='Cancel'
        cancelClick={this.cancelClick}
        handleSubmit={this.handleSubmit}
      />
    );
  }
}

const mapActionsToProp = (dispatch) => ({
  addPizzaType(product) {
    return dispatch(addPizzaType(product))
  },
  addFries(fries) {
    return dispatch(addFries(fries));
  },
  addChicken(chicken) {
    return dispatch(addChicken(chicken));
  }
})

export const AddPriceListItemContainer = connect(
  null,
  mapActionsToProp,
)(_AddPriceListItemContainer)
