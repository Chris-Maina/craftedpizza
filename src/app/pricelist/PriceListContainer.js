import React, { Component } from 'react';
import { View, StyleSheet } from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import { CardComponent, TextComponent } from '../common';

export class PriceListContainer extends Component {
  static navigationOptions = {
    headerStyle: {
      backgroundColor: '#EF4DB6',
    },
  };
  render() {
    const { navigation: { navigate } } = this.props;
    return (
      <LinearGradient colors={['#EF4DB6', '#C643FC']} style={styles.container}>
        <TextComponent text="Price list" style={styles.priceTitle} />
        <View style={styles.cardContainer}>
          <CardComponent 
            style={styles.card}
            onPress={() => navigate('PizzaPrices')}>
            <TextComponent text="Pizza" style={styles.cardText} />
          </CardComponent>
          <CardComponent 
            style={styles.card}
            onPress={() => navigate('FriesPrices')}
          >
            <TextComponent text="Fries" style={styles.cardText} />
          </CardComponent>
        </View>
        <View style={styles.cardContainer}>
          <CardComponent
            style={styles.card}
            onPress={() => navigate('ChickenPrices')}
            >
            <TextComponent text="Chicken" style={styles.cardText} />
          </CardComponent>
          <CardComponent
            style={styles.card}
            onPress={() => navigate('BeveragePrices')}
            >
            <TextComponent text='Beverages' style={styles.cardText} />
          </CardComponent>
        </View>
      </LinearGradient>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "space-around"
  },
  priceTitle: {
    textAlign: "center",
    color: "white",
    fontSize: 36
  },
  cardContainer: {
    flexDirection: "row",
    justifyContent: "space-around"
  },
  card: {
    width: 150,
    height: 150,
    padding: 20,
    alignItems: "center",
    justifyContent: "center",
    borderColor: "rgba(0, 0 ,0 ,0.3)",
    borderRadius: 27,
    shadowColor: "#EF4DB6",
    shadowRadius: 10
  },
  cardText: {
    color: "white",
    fontSize: 22
  }
});
