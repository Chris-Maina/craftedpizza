import types from './types'

const getAllChickenRequest = () => ({
  type: types.GET_ALL_CHICKEN_REQUEST
});

const getAllChickenSuccess = (chicken) => ({
  type: types.GET_ALL_CHICKEN_SUCCESS,
  payload: chicken
});

const getAllChickenError = () => ({
  type: types.GET_ALL_CHICKEN_ERROR
});

const addChickenRequest = () => ({
  type: types.ADD_CHICKEN_REQUEST
});

const addChickenSuccess = (chicken) => ({
  type: types.ADD_CHICKEN_SUCCESS,
  payload: chicken
});

const addChickenError = () => ({
  type: types.ADD_CHICKEN_ERROR
});

export default {
  getAllChickenError,
  getAllChickenRequest,
  getAllChickenSuccess,
  addChickenError,
  addChickenRequest,
  addChickenSuccess,
}
