import initialState from '../../../../reducers/initialState';
import types from './types';

export const chickenList = (state = initialState.priceLists.chickenList, { type, payload }) => {
  switch (type) {
    case types.GET_ALL_CHICKEN_REQUEST: 
      return {
        ...state,
        loading: true,
        hasError: false
      }
    case types.GET_ALL_CHICKEN_SUCCESS:
      return {
        ...state,
        chicken: payload,
        loading: false,
        hasError: false
      }
    case types.GET_ALL_CHICKEN_ERROR: 
      return {
        ...state,
        loading: false,
        hasError: true,
      }
    case types.ADD_CHICKEN_REQUEST: 
      return {
        ...state,
        loading: true,
        hasError: false
      }
    case types.ADD_CHICKEN_SUCCESS:
      return {
        ...state,
        chicken: [...payload, state.chicken],
        loading: false,
        hasError: false
      }
    case types.ADD_CHICKEN_ERROR: 
      return {
        ...state,
        loading: false,
        hasError: true,
      }
    default:
      return state;
  }
}
