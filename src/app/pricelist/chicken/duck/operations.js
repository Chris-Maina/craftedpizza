import actions from './actions';
import { get, post } from '../../../../services/api';

export const getAllChicken = () => (
  async dispatch => {
    try {
      dispatch(actions.getAllChickenRequest());
      const allChicken = await get('product?type=chicken');
      dispatch(actions.getAllChickenSuccess(allChicken));
    } catch (error) {
      dispatch(actions.getAllChickenError());
    }
  }
);

export const addChicken = (product) => (
  async dispatch => {
    try {
      dispatch(actions.addChickenRequest());
      const addedChicken = await post('product', product);
      dispatch(actions.addChickenSuccess(addedChicken));
    } catch (error) {
      dispatch(actions.addChickenError());
    }
  }
);
