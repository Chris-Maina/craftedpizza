import React, { Component } from 'react';
import { connect } from 'react-redux';
import { ItemListContainer } from '../ItemListContainer';
import { getAllChicken } from '../chicken/duck';

export class _ChickenPriceListContainer extends Component {
  static navigationOptions = {
    title: 'Chicken',
  };

  componentDidMount() {
    this.props.getAllChicken();
  }

  handleAddClick = () => {
    const { navigation: { navigate }} = this.props;
    navigate('Add', { priceListType: 'chicken' });
  };

  handleItemClick = (item) => {
    const { navigation: { navigate } } = this.props;
    navigate('ProductDetails', { item, title: 'Chicken' } );
  };

  render() {
    const { chicken } = this.props;
    return (
      <ItemListContainer
        items={chicken}
        handleAddClick={this.handleAddClick}
        handleItemClick={this.handleItemClick}
      />
    ); 
  }
}

const mapStateToProps = ({ chickenList }) => ({
  chicken: chickenList.chicken,
  loading: chickenList.loading,
  hasError: chickenList.hasError
});

const mapActionsToProps = dispatch => ({
  getAllChicken() {
    return dispatch(getAllChicken());
  }
});

export const ChickenPriceListContainer = connect(mapStateToProps, mapActionsToProps)(_ChickenPriceListContainer);
