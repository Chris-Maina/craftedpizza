import { createOptions } from './createOptions';

const options = ['No', 'Extra cheese', 'Extra toppings', 'Both'];
export const extras = createOptions(options, 'extras');
