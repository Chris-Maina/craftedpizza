import { createOptions } from './createOptions';

const options = ['Large', 'Medium', 'Small']
export const sizes = createOptions(options, 'sizes');
