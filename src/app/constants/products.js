import { createOptions } from './createOptions';

const options = ['Pizza', 'Pizza Slices', 'Fries', 'Chicken'];
export const products = createOptions(options, 'products')
