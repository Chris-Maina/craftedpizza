import { createOptions } from './createOptions';

const deliveryOptions = ['Paid delivery' , 'Free delivery', 'Walk in'];
export const deliveryTypes = createOptions(deliveryOptions, 'deliveryTypes');
