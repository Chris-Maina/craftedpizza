export const createOptions = (options, key) => (
  options.map( (option, index) => ({
    id: option.id || index,
    title: option.title ? option.title: option ,
    selected: false,
    key: option.key ? option.key : key,
  }))
);
