import gql from "graphql-tag";

export const CUSTOMER_FRAGMENT = gql`
  fragment Customer on Customer {
    id
    name
    phoneNumber
    comments
  }
`