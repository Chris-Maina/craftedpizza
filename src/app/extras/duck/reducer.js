import types from './types';
import initialState from '../../../reducers/initialState';

export const extrasReducer = (state=initialState.extrasReducer, { type, payload }) => {
  switch(type) {
    case types.GET_ALL_EXTRAS_REQUEST:
      return {
        ...state,
        loading: true,
        hasError: false
      }
    case types.GET_ALL_EXTRAS_SUCCESS:
      return {
        ...state,
        extras: payload,
        loading: true,
        hasError: false
      }
    case types.GET_ALL_EXTRAS_ERROR:
      return {
        ...state,
        loading: false,
        hasError: true
      }
    default: 
      return state;
  }
}
