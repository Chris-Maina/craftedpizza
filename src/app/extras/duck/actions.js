import types from './types';

const getAllExtrasRequest = () => ({
    type: types.GET_ALL_EXTRAS_REQUEST
});

const getAllExtrasSuccess = (extras) => ({
    type: types.GET_ALL_EXTRAS_SUCCESS,
    payload: extras
});

const getAllExtrasError = () => ({
  type: types.GET_ALL_EXTRAS_ERROR
});

export default {
  getAllExtrasRequest,
  getAllExtrasSuccess,
  getAllExtrasError
}
