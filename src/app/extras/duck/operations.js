import { get } from '../../../services/api';
import {
  takeLatest,
  call,
  put
} from 'redux-saga/effects';
import types from './types';
import actions from './actions';

export function* watchGetAllExtras () {
  yield takeLatest(types.GET_ALL_EXTRAS_REQUEST, getAllExtras);
}

function* getAllExtras () {
  try {
    const extras = yield call(get, 'extras');
    yield put(actions.getAllExtrasSuccess(extras));
  } catch (error) {
    yield put(actions.getAllExtrasError());
  }
}
