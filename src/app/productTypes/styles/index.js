import { StyleSheet } from 'react-native';

const button = {
  height: 47,
  marginTop: 10,
  borderRadius: 15,
}
const text = {
  fontSize: 16,
  alignSelf: "center",
  marginTop: "auto",
  marginBottom: "auto"
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 20,
    paddingLeft: 10,
    paddingRight: 10,
    backgroundColor: "#FFF5EE"
  },
  titleText: {
    fontSize: 17,
    fontWeight: "400",
    paddingLeft: 3,
    paddingBottom: 10,
    color: "#442C2E"
  },
  addButton: {
    ...button,
    backgroundColor: "#09A494"
  },
  cancelButton: {
    ...button,
    backgroundColor: "#fff"
  },
  buttonText: {
    color: "white",
   ...text,
  },
  cancelText: {
    color: "#442C2E",
   ...text,
  },
  errorMessage: {
    color: "red",
    marginTop: 5,
  },
  category: {
    height: 40,
    padding: 10,
    backgroundColor: "#ffffff",
    borderRadius: 7,
    color: "#442C2E",
    borderWidth: 1,
    borderStyle: "solid",
    borderColor: "rgba(0, 0, 0, 0.2)"
  }
});

export default styles;
