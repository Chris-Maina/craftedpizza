import gql from "graphql-tag";

export const PRODUCT_TYPE_FRAGMENT = gql`
  fragment ProductType on ProductType {
    id
    title
    key
  }
`;

export const GET_PRODUCT_TYPES = gql`
  query getProductTypes {
    productTypes {
      ...ProductType
    }
  }
  ${PRODUCT_TYPE_FRAGMENT}
`;

export const ADD_PRODUCT_TYPE = gql`
  mutation createProductType($title: String) {
    createProductType(title: $title) {
      ...ProductType
    }
  }
  ${PRODUCT_TYPE_FRAGMENT}
`;

export const DELETE_PRODUCT_TYPE = gql`
  mutation deleteProductType($id: Int) {
    deleteProductType(id: $id)
  }
`;
