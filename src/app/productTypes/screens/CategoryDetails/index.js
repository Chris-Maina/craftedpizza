import React, { Component } from "react";
import { Mutation } from "react-apollo";
import { View } from "react-native";

import {
  ListItemComponent,
  TextComponent,
  ButtonComponent,
  IconComponent
} from "../../../common";

import styles from "./styles";
import { DELETE_PRODUCT_TYPE, GET_PRODUCT_TYPES } from "../../duck";

class CategoryDetails extends Component {
  static navigationOptions = {
    title: "Details",
    headerStyle: {
      backgroundColor: "#FFF5EE",
      border: 0
    }
  };

  handleDeleteClick = mutation => {
    const { category } = this.props.navigation.state.params;

    mutation({ variables: { id: category.id }});
  }

  handleEditClick = () => {
    const { category } = this.props.navigation.state.params;
    this.props.navigation.navigate('EditProduct', { category });
  }

  render() {
    const { navigation } = this.props;
    const { category } = navigation.state.params;

    return (
      <Mutation
        mutation={DELETE_PRODUCT_TYPE}
        update={cache => {
          const { productTypes } = cache.readQuery({
            query: GET_PRODUCT_TYPES
          });
          cache.writeQuery({
            query: GET_PRODUCT_TYPES,
            data: { productTypes: productTypes.filter(el => el.id !== category.id) }
          });
        }}
      >
        {deleteProduct => (
          <View style={styles.container}>
            <ListItemComponent style={styles.card}>
              <View style={styles.headerContainer}>
                <TextComponent
                  style={styles.header}
                  text='Category'
                />
                <ButtonComponent
                  style={styles.deleteButton}
                  onPress={() => this.handleDeleteClick(deleteProduct)}
                >
                  <IconComponent
                    name="delete"
                    size={30}
                    color="white"
                    style={styles.icon}
                  />
                </ButtonComponent>
              </View>
              <View>
                <TextComponent style={styles.labelText} text={category.title} />
              </View>
            </ListItemComponent>
            <ButtonComponent
              style={styles.button}
              onPress={this.handleEditClick}
            >
              <IconComponent
                name="edit"
                size={40}
                color="white"
                style={styles.icon}
              />
            </ButtonComponent>
          </View>
        )}
      </Mutation>
    );
  }
}

export default CategoryDetails;
