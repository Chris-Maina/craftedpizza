import React, { Component } from 'react';
import { Mutation } from 'react-apollo';

import { FormComponent } from '../../../common';

import { ADD_PRODUCT_TYPE, GET_PRODUCT_TYPES } from '../../duck';

import createField from '../../../../services/createField';
import validateFormFields from '../../../../services/formValidator';

class AddProductTypeContainer extends Component {

  static navigationOptions = {
    title: 'Add product type'
  }

  constructor(props) {
    super(props);
    this.state = {
      title: '',
      errors: {}
    }
  }

  /**
   * @name handleTitleChange
   * @param {string} title
   */
  handleTitleChange = (title) => {
    this.setState({ title });
  }

  /**
   * @name handleSubmit
   * @description Handles submitting of a product type
   * @param {func} mutation
   */
  handleSubmit = (mutation) => {
    const { title } = this.state;
    const { navigation: { navigate } } = this.props;
    const errors = validateFormFields(this.state, ['title']);
    if (Object.keys(errors).length === 0) {
      mutation({ variables: { title }});
      navigate('ProductTypes');
    } else {
      this.setState({ errors });
    }
  }

  /**
   * @name renderValidationError
   * @description sets errors state of each form field
   * @param {string} field
   */
  renderValidationError = (field) => {
    if (typeof this.state.errors[field] !== 'undefined') {
      return this.state.errors[field];
    }
    return '';
  }

  /**
   * @name cancelClick
   * @description clears the form fields
   */
  cancelClick = () => {
    this.setState({ title: '' });
  }

  render() {
    const { title } = this.state
    const fields = [createField(
      'Product type',
      'input',
      'Enter product type',
      title,
      null,
      this.handleTitleChange,
      this.renderValidationError
    )];
    return (
      <Mutation
        mutation={ADD_PRODUCT_TYPE}
        update={
          (cache, { data: { createProductType } }) => {
            const { productTypes } = cache.readQuery({ query: GET_PRODUCT_TYPES });
            cache.writeQuery({
              query: GET_PRODUCT_TYPES,
              data: { productTypes: [...productTypes, createProductType]}
            });
          }
        }
      >
        {
          createProductType => (
            <FormComponent
              fields={fields}
              iconName='check'
              cancelText='Clear'
              cancelClick={this.cancelClick}
              handleSubmit={() => this.handleSubmit(createProductType)}
            />
          )
        }
      </Mutation>
    )
  }
}

export default AddProductTypeContainer;
