import React, { Component } from 'react';
import { Query } from 'react-apollo';

import { Content } from './Content';

import { GET_PRODUCT_TYPES } from './duck';
import validateFormFields from '../../services/formValidator';

class ProductTypesContainer extends Component {
  static navigationOptions = {
    title: 'Categories',
  };

  state = {
    category: '',
    errors: {},
    viewCategoryInput: false,
  }

  toggleViewCategory = bool => {
    this.setState({ viewCategoryInput: bool });
  }

  handleAddClick = () => {
    this.toggleViewCategory(true);
  }

  handleCategoryClick = category => {
    this.props.navigation.navigate('CategoryDetails', { category });
  }

  handleCategoryChange = category => {
    this.setState({ category, formErrors: { category: '' } })
  }

  handleCancelClick = () => {
    this.toggleViewCategory(false);
    this.setState({ category: '', formErrors: { category: ''} });
  }

  handleCategorySubmit = mutation => {
    const { category } = this.state;

    const errors = validateFormFields(this.state, ['category']);
    if (Object.keys(errors).length === 0) {
      mutation({ variables: { title: category }});
      this.toggleViewCategory(false);
    } else {
      this.setState({ errors });
    }
  }

  render () {
    const { viewCategoryInput, errors } = this.state
    return (
      <Query query={GET_PRODUCT_TYPES}>
        {
          ({ loading, error, data, refetch }) => {
            return (
              <Content
                error={error}
                refetch={refetch}
                loading={loading}
                formErrors={errors}
                productTypes={data.productTypes}
                handleAddClick={this.handleAddClick}
                viewCategoryInput={viewCategoryInput}
                handleCancelClick={this.handleCancelClick}
                handleCategoryClick={this.handleCategoryClick}
                handleCategoryChange={this.handleCategoryChange}
                handleCategorySubmit={this.handleCategorySubmit}
              />
            );
          }
        }
      </Query>
    );
  }
}

export default ProductTypesContainer;
