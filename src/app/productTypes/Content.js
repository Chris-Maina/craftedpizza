import React, { Component } from "react";
import { Mutation } from "react-apollo";
import {
  View,
  Text,
  TextInput,
  ScrollView,
  ActivityIndicator
} from "react-native";

import styles from "./styles";
import { ProductTypesList } from "./components";
import { ButtonComponent, TextComponent, ErrorComponent } from "../common";

import { ADD_PRODUCT_TYPE, GET_PRODUCT_TYPES } from "./duck";

export class Content extends Component {
  renderAddCategoryButton = () => {
    const { handleAddClick } = this.props;
    return (
      <ButtonComponent style={styles.addButton} onPress={handleAddClick}>
        <TextComponent text="Add Category" style={styles.buttonText} />
      </ButtonComponent>
    );
  };

  renderCategoryInput = () => {
    const {
      category,
      formErrors,
      handleCategorySubmit,
      handleCategoryChange,
      handleCancelClick
    } = this.props;
    return (
      <Mutation
        mutation={ADD_PRODUCT_TYPE}
        update={(cache, { data: { createProductType } }) => {
          const { productTypes } = cache.readQuery({
            query: GET_PRODUCT_TYPES
          });
          cache.writeQuery({
            query: GET_PRODUCT_TYPES,
            data: { productTypes: [...productTypes, createProductType] }
          });
        }}
      >
        {createProductType => (
          <View>
            <TextInput
              value={category}
              style={styles.category}
              clearButtonMode="always"
              placeholder="Enter category name"
              onChangeText={text => handleCategoryChange(text)}
            />
            {formErrors.category && (
              <Text style={styles.errorMessage}>{formErrors.category}</Text>
            )}
            <ButtonComponent
              style={styles.addButton}
              onPress={() => handleCategorySubmit(createProductType)}
            >
              <TextComponent text="Submit" style={styles.buttonText} />
            </ButtonComponent>
            <ButtonComponent
              style={styles.cancelButton}
              onPress={handleCancelClick}
            >
              <TextComponent text="Cancel" style={styles.cancelText} />
            </ButtonComponent>
          </View>
        )}
      </Mutation>
    );
  };
  render() {
    const {
      error,
      loading,
      refetch,
      productTypes,
      viewCategoryInput,
      handleCategoryClick
    } = this.props;

    if (loading) return <ActivityIndicator size="large" color="#EF4DB6" />;
    if (error)
      return (
        <ErrorComponent
          message="There was an error loading product types"
          retry={() => refetch()}
        />
      );

    return (
      <ScrollView style={styles.container}>
        <ProductTypesList
          types={productTypes}
          handleCategoryClick={handleCategoryClick}
        />
        {viewCategoryInput
          ? this.renderCategoryInput()
          : this.renderAddCategoryButton()}
      </ScrollView>
    );
  }
}
