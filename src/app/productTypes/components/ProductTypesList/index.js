import React from "react";
import { StyleSheet } from "react-native";

import { ListItemComponent, TextComponent } from "../../../common/index";

const ProductTypesListComponent = props => {
  const { types, handleCategoryClick } = props;
  return types.map(item => (
    <ListItemComponent
      style={styles.card}
      key={item.id}
      onPress={() => handleCategoryClick(item)}
    >
      <TextComponent style={styles.cardText} text={item.title} />
    </ListItemComponent>
  ));
};

const styles = StyleSheet.create({
  card: {
    height: 47
  },
  cardText: {
    fontSize: 14,
    color: "#442C2E",
    alignSelf: "center",
    textTransform: "capitalize"
  }
});

export default ProductTypesListComponent;
