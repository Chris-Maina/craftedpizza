export const DELIVERY_TYPES = {
  PAID: 'Paid delivery',
  WALKIN: 'Walk in',
  FREE: 'Free delivery'
};
