import { Dimensions, StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  headerStyle: {
    backgroundColor: '#FFF5EE',
  },
  container: {
    backgroundColor: '#FFF5EE',
    flex: 1
  },
  orderContainer: {
    flexDirection: 'row',
    paddingTop: 10,
    fontSize: 12,
    color: '#2E2E2E',
  },
  orderNumber: {
    paddingLeft: 8
  },
  cardContainer: {
    width: 310,
    padding: 5,
    marginLeft: 20,
    backgroundColor: '#ffffff',
    borderRadius: 2,
    borderWidth: 0,
    shadowOffset: { width: 2, height: 2 },
    shadowOpacity: 0.1
  },
  text: {
    marginTop: 3,
    marginLeft: 5
  },
  hr: {
    height: 1.5,
    marginTop: 20,
    borderColor: '#D1D1D1',
    backgroundColor: '#D1D1D1',
    width: Dimensions.get('window').width - 23,
  },
  addButton: {
    alignSelf: 'flex-end',
    position: 'absolute',
    zIndex: 1,
    bottom: 28,
    right: 10,
    borderRadius: 62.1,
    width: 62.1,
    height: 62.1,
    backgroundColor: '#09A494',

  },
  icon: {
    alignSelf: 'center',
    marginTop: 'auto',
    marginBottom: 'auto'
  }
});

export default styles;
