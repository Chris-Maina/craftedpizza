import React from 'react';
import { Query } from 'react-apollo';
import {
  View,
  ScrollView,
  TouchableOpacity,
  ActivityIndicator,
} from 'react-native';

import {
  TextComponent,
  IconComponent,
  ErrorComponent,
  ButtonComponent,
} from '../common';
import styles from './styles';
import { ProductCard, DeliveryCard, CustomerCard } from './components';
import { GET_ORDERS } from './duck/orders.query';

const Content = (props) => {
  const { handleOrderClick, handleAddOrderClick } = props;
  return (
    <Query query={GET_ORDERS}>
      {
        ({ loading, error, data, refetch }) => {
          if(loading) return (<ActivityIndicator size='large' color='#EF4DB6' />);
          if(error) return (<ErrorComponent message='There was an error loading data.' retry={() => refetch()} />);
          const { orders } = data;
          return (
            <ScrollView style={styles.container}>
              <View>
                {
                  /**
                   * Search bar and toggle button
                   */
                }
              </View>
              {
                orders.map(order => (
                  <View key={order.id} style={styles.orderContainer}>
                    <TextComponent style={styles.orderNumber} text={order.id} />
                    <TouchableOpacity
                      onPress={() => handleOrderClick(order)}
                     >
                      <ProductCard
                        text={styles.text}
                        cardContainer={styles.cardContainer}
                        date={order.createdAt}
                        orderProduct={order.orderProduct}
                      />
                      {
                        order.delivery && order.delivery.location &&
                          <DeliveryCard
                            text={styles.text}
                            cardContainer={styles.cardContainer}
                            delivery={order.delivery}
                          />
                      }
                      {
                        order.customer && Object.keys(order.customer).length &&
                          <CustomerCard
                            text={styles.text}
                            cardContainer={styles.cardContainer}
                            customer={order.customer}
                          />
                      }
                      <View style={styles.hr} />
                    </TouchableOpacity>
                  </View>
                ))
              }
              <ButtonComponent
                onPress={handleAddOrderClick}
                style={styles.addButton}>
                <IconComponent
                  name='add'
                  size={40}
                  color='white'
                  style={styles.icon}
                  />
              </ButtonComponent>
            </ScrollView>
          );
        }
      }
    </Query>
  )
}

export default Content;
