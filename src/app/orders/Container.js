import React, { Component } from 'react';
import Content from './Content';
import styles from './styles';

class Container extends Component {

  static navigationOptions = {
    title: 'Orders',
    headerStyle: {
      ...styles.headerStyle,
      border: 0,
    },
  };

  handleOrderClick = (order) => {
    const { navigation: { navigate }} = this.props;
    navigate('SingleOrder', { order });
  }

  handleAddOrderClick = () => {
    const { navigation: { navigate }} = this.props;
    navigate('AddOrder');
  }
  
  handEditClick = () => {
    const { selectedItem } = this.state;
    const { navigation: { navigate }} = this.props;
    navigate('Edit', { order: selectedItem });
  }

  render() {
    return(
      <Content
        handleOrderClick={this.handleOrderClick}
        handleAddOrderClick={this.handleAddOrderClick}
      />
    );
  }
}

export default Container;
