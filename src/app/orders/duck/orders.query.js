import gql from "graphql-tag";
import { PRODUCT_FRAGMENT } from '../../products/duck';
import { CUSTOMER_FRAGMENT } from '../../customers/duck';
import { DELIVERY_FRAGMENT } from '../../deliveries/duck';
import { PRODUCT_TYPE_FRAGMENT } from '../../productTypes/duck';

export const GET_PRODUCTS_AND_TYPES = gql`
  {
    products {
      id
      name
      size
      price
      product_type {
        id
        title
        key
      }
    }
    productTypes {
      id
      title
      key
    }
  }
`;

const ORDER_PRODUCTS_FRAGMENT = gql`
  fragment OrderProducts on OrderProduct {
    id
    quantity
    product {
      ...Product
      product_type {
        ...ProductType
      }
    }
  }
  ${PRODUCT_FRAGMENT}
  ${PRODUCT_TYPE_FRAGMENT}
`

export const GET_ORDERS = gql`
query getOrders {
  orders {
    id
    createdAt
    customer {
      ...Customer
    }
    delivery {
      ...Delivery
    }
    orderProduct {
      ...OrderProducts
    }
  }
}
  ${CUSTOMER_FRAGMENT}
  ${DELIVERY_FRAGMENT}
  ${ORDER_PRODUCTS_FRAGMENT}
`

export const ADD_ORDER = gql`
mutation saveOrder($customer:CustomerInput,$delivery: DeliveryInOrderInput, $customerOrders: [CustomerOrderInput]) {
  saveOrder(customer:$customer, delivery: $delivery, customerOrders: $customerOrders)
  {
    id
    createdAt
    customer {
      ...Customer
    }
    delivery {
      ...Delivery
    }
    orderProduct {
      id
      quantity
      product {
        ...Product
      }
    }
  }
}
${CUSTOMER_FRAGMENT}
${DELIVERY_FRAGMENT}
${PRODUCT_FRAGMENT}
`

export const UPDATE_ORDER = gql`
mutation updateOrder($customer: CustomerInput, $delivery: DeliveryInOrderInput, $customerOrders: [CustomerOrderInput]) {
  updateOrder(customer: $customer, delivery: $delivery, customerOrders: $customerOrders)
  {
    id
    customer {
      ...Customer
    }
    delivery {
      ...Delivery
    }
    orderProduct {
      id
      quantity
      product {
        ...Product
      }
    }
  }
}
${CUSTOMER_FRAGMENT}
${DELIVERY_FRAGMENT}
${PRODUCT_FRAGMENT}
`

export const DELETE_ORDER = gql`
mutation deleteOrder($id: Int) {
  deleteOrder(id: $id) 
}
`
