import React, { Component } from 'react';
import { View } from 'react-native';

import {
  TextComponent,
  DropdownComponent,
  TextInputComponent
} from '../../../common';

import styles from '../styles';

class DeliveryFieldsComponent extends Component {

  renderDropdownInput = (name, options, title) => {
    const { toggleSelectedOption, renderValidationError } = this.props;
    return (
      <View style={styles.inputContainer}>
        <DropdownComponent
          options={options}
          title={title}
          toggleItem={toggleSelectedOption}
        />
        <TextComponent
          text={renderValidationError(name)}
          style={styles.errorMessage}
        />
      </View>
    );
  };

  renderTextInput = (name, title, value, keyboardType) => {
    const { handleChange, renderValidationError } = this.props;
    return(
      <View style={styles.inputContainer}>
        <TextInputComponent
          placeholder={title}
          value={value}
          keyboardType={keyboardType}
          onChangeText={text => handleChange(name, text)}
        />
        <TextComponent
          text={renderValidationError(name)}
          style={styles.errorMessage}
        />
      </View>
    );
  }

  render(){
    const {
      cost,
      time,
      location,
      deliveryTypes,
      isWalkinSelected,
      deliveryTypeTitle,
      isPaidDeliverySelected,
    } = this.props
    return (
      <View>
          {this.renderDropdownInput('type', deliveryTypes, deliveryTypeTitle )}
          {
            !isWalkinSelected &&
              this.renderTextInput('location', 'Enter delivery location', location)
          }
          {
            isPaidDeliverySelected && 
              this.renderTextInput('cost', 'Enter delivery cost', cost, 'numeric')
          }
          {this.renderTextInput('time', 'Enter delivery time desired', time, 'numeric')}
        </View>
    );
  }
}

export default DeliveryFieldsComponent;
