import React from 'react';
import { View } from 'react-native';

import { TextComponent } from '../../../common';
import styles from '../styles';


const DeliveryCardComponent = ({ cardContainer, text, delivery }) => {
  const { location, cost } = delivery;
  return (
    <View style={[cardContainer, styles.container]}>
      {location && <TextComponent style={text} text={`Delivery location:  ${location}`} />}
      {cost && <TextComponent style={text} text={`Delivery cost: KES ${cost}`} />}
    </View>
  );
};

export default DeliveryCardComponent;
