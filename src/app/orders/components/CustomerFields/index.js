import React from 'react';
import { View } from 'react-native'; 

import styles from '../styles';
import { TextComponent, TextInputComponent } from '../../../common';

const CustomerFieldsComponent = (
  {
    name,
    comments,
    phoneNumber,
    handleChange,
    renderValidationError,
  }
) => {
  return (
    <View>
        <View style={styles.inputContainer}>
          <TextInputComponent
            name='name'
            onChangeText={text => handleChange('name', text)}
            placeholder='Enter customer name'
            value={name}
          />
          <TextComponent
            text={renderValidationError('name')}
            style={styles.errorMessage}
          />
        </View>
        <View style={styles.inputContainer}>
          <TextInputComponent
            name='phoneNumber'
            onChangeText={text => handleChange('phoneNumber', text)}
            placeholder='Enter phone number'
            value={phoneNumber}
          />
          <TextComponent
            text={renderValidationError('phoneNumber')}
            style={styles.errorMessage}
          />
        </View>
        <View style={styles.inputContainer}>
          <TextInputComponent
            multiline={true}
            name='comments'
            onChangeText={text => handleChange('comments', text)}
            placeholder='Enter customer comment(s)'
            value={comments}
          />
          <TextComponent
            text={renderValidationError('comments')}
            style={styles.errorMessage}
          />
        </View>
      </View>
  );
}

export default CustomerFieldsComponent;
