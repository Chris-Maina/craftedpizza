import React from 'react';
import { View } from 'react-native';

import { TextComponent } from '../../../common';
import styles from '../styles';

const CustomerCardComponent = ({ cardContainer, text, customer }) => {
  const { name, phoneNumber } = customer;
  return (
  <View style={[cardContainer, styles.container]}>
    {phoneNumber && <TextComponent style={text} text={`Customer phone:  ${phoneNumber}`} />}
    {name && <TextComponent style={text} text={`Customer name: ${name}`} />}
  </View>
);
}

export default CustomerCardComponent;
