import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  totalCostContainer: {
    height: 40,
    padding: 10,
    backgroundColor: '#ffffff',
    shadowOpacity: 0.2,
    borderWidth: 0,
    marginBottom: 20
  },
  textContainer: {
    flexDirection: 'row',
    shadowOpacity: 0,
   
  },
  costText: {
    color: '#FF9900',
    marginLeft: 10
  }
});

export default styles;
