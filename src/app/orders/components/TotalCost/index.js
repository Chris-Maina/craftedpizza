import React from 'react';
import { View } from 'react-native';

import styles from './styles';
import { CardComponent, TextComponent } from '../../../common';

const TotalCostComponent = ({ total }) => (
  <CardComponent style={styles.totalCostContainer}>
    <View style={styles.textContainer}>
      <TextComponent style={styles.totalCostText} text='Total cost: ' />
      <TextComponent style={styles.costText} text={total} />
    </View>
  </CardComponent>
);

export default TotalCostComponent;
