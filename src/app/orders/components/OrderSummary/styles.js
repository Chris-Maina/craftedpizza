import { StyleSheet } from 'react-native'; 

const styles = StyleSheet.create({
  container: {
    padding: 10,
    backgroundColor: '#ffffff',
    shadowOpacity: 0.2,
    borderWidth: 0,
    marginBottom: 20
  },
  textContainer: {
    color: '#442C2E',
    paddingTop: 5,
    paddingBottom: 5,
    flexDirection: 'row',
  },
  text: {
    textAlign: 'left',
    marginLeft: 5
  },
  buttonActions: {
    marginLeft: 'auto'
  }
});

export default styles;
