import React, { Component } from 'react';
import { View } from 'react-native'; 

import {
  CardComponent,
  TextComponent,
  IconComponent,
  ButtonComponent
} from '../../../common';

import styles from './styles';

class OrderSummaryComponent extends Component {

  // /**
  //  * @name handleEditClick
  //  * @param {Object} - product
  //  * @param {Int} - quantity
  //  */
  // handleEditClick = (product, quantity) => {
  //   const { onEditClick } = this.props;
  //   onEditClick(product, quantity);
  // }

  /**
   * @name handleDeleteClick
   * @param {Int} - product id
   */
  handleDeleteClick = (productId) => {
    const { onDeleteClick } = this.props;
    onDeleteClick(productId);
  }

  /**
   * @name renderOrderedProducts
   * @param {Array} - orders - Customer productIds and their quantities
   * @param {Array} - products - products
   */
  renderOrderedProducts = (orders, products) => {
    return orders.map((order, index) => {
      const { quantity, productId } = order;
      const product = productId ? products.find(prod => prod.id === productId): order.product;
      return (
        <View key={index} style={styles.textContainer}>
          {quantity &&  <TextComponent style={styles.text}  text={quantity} />}
          {product.name && <TextComponent style={styles.text}  text={product.name} />}
          {product.size && <TextComponent style={styles.text}  text={product.size} />}
          {product.price && <TextComponent style={styles.text}  text={product.price} />}
          {/* <ButtonComponent
            onPress={() => this.handleEditClick(product, quantity)}
            style={styles.buttonActions}
          >
            <IconComponent
              name='edit'
              size={20}
              color='#442C2E'
              style={styles.icon}
            />
          </ButtonComponent> */}
          <ButtonComponent
            onPress={() => this.handleDeleteClick(product.id)}
            style={styles.buttonActions}
          > 
            <IconComponent
              name='delete'
              size={20}
              color='red'
            />
          </ButtonComponent>
        </View>
      )
    })
  }

  render() {
    const { orders, products } = this.props;
    return (
      <CardComponent style={styles.container}>
       {this.renderOrderedProducts(orders, products)}
      </CardComponent>
      );
  }
};

export default OrderSummaryComponent;
