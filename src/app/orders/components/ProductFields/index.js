import React, { Component } from 'react';
import { View } from 'react-native';

import {
  TextComponent,
  DropdownComponent,
  TextInputComponent
} from '../../../common';

import styles from '../styles';

class ProductFieldsComponent extends Component {
  renderDropdownInput = (name, options, title) => {
    const { toggleSelectedOption, renderValidationError } = this.props;
    return (
      <View style={styles.inputContainer}>
        <DropdownComponent
          options={options}
          title={title}
          toggleItem={toggleSelectedOption}
        />
        <TextComponent
          text={renderValidationError(name)}
          style={styles.errorMessage}
        />
      </View>
    );
  };
  render() {
    const {
      productTypesTitle,
      productTypes,
      productTitle,
      products,
      quantity,
      handleChange,
      renderValidationError
    } = this.props;

    return (
      <View>
        {this.renderDropdownInput(
          'productType',
          productTypes,
          productTypesTitle
        )}
        {this.renderDropdownInput('productId', products, productTitle)}
        <View>
          <TextInputComponent
            onChangeText={text => handleChange('quantity', text)}
            placeholder={productTypesTitle.toLowerCase() === 'chicken' ? 'Weight' : 'Quantity'}
            value={quantity}
            keyboardType='numeric'
          />
          <TextComponent
            text={renderValidationError('quantity')}
            style={styles.errorMessage}
          />
        </View>
      </View>
    );
  }
}

export default ProductFieldsComponent;
