import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  container: {
    marginTop: 10,
  },
  detailsContainer: {
    flexDirection: 'row',
  },
  dateText: {
    marginBottom: 8
  },
  inputContainer: {
    marginBottom: 10
  },
  label: {
    fontSize: 18,
    marginBottom: 5,
    color: '#442C2E',
  },
  errorMessage: {
    color: "red",
    textTransform: 'capitalize'
  }
});

export default styles;
