import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  buttonActions: {
    marginLeft: 'auto',
    flexDirection: 'row',
  }
});

export default styles;
