import React from 'react';
import { View } from 'react-native';

import styles from './styles';
import { IconComponent, ButtonComponent } from '../../../common';


const EditDeleteButtons = props => {
  const { handleEditProductClick, handleDeleteClick, product, quantity, id } = props;
  return (
  <View style={styles.buttonActions}>
    <ButtonComponent onPress={() => handleEditProductClick(id, product, quantity)}>
      <IconComponent name="edit" size={20} color="#442C2E" />
    </ButtonComponent>
    <ButtonComponent onPress={() => handleDeleteClick(id, product.id)}>
      <IconComponent name="delete" size={20} color="red" />
    </ButtonComponent>
  </View>
);
}

export default EditDeleteButtons;
