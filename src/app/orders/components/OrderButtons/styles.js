import { StyleSheet } from 'react-native'; 

const styles = StyleSheet.create({
  buttonContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    marginBottom: 20
  },
  button: {
    borderRadius: 8,
    width: 115.6,
    height: 40,
    backgroundColor: '#09A494',
  },
  cancelButton: {
    borderRadius: 8,
    width: 115.6,
    height: 40,
    backgroundColor: '#FFFFFF',
    
  },
  buttonText: {
    alignSelf: 'center',
    marginTop: 'auto',
    marginBottom: 'auto',
    fontSize: 18
  },
  saveText: {
    color: '#ffffff',
  },
  clearText: {
    color: '#442C2E',
  }
});

export default styles;
