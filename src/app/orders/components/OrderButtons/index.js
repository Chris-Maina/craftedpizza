import React from 'react';
import { View } from 'react-native';

import styles from './styles';
import { ButtonComponent, TextComponent } from '../../../common';

export const OrderButtonsComponent = ({ clearText, saveText, clearClick, handleSubmit }) => (
  <View style={styles.buttonContainer}>
    <ButtonComponent style={styles.cancelButton} onPress={clearClick}>
      <TextComponent
        text={clearText}
        style={[styles.buttonText, styles.clearText]}
        />
    </ButtonComponent>
    <ButtonComponent style={styles.button} onPress={handleSubmit}>
      <TextComponent
        text={saveText}
        style={[styles.buttonText, styles.saveText]}
        />
    </ButtonComponent>
  </View>
);

export default OrderButtonsComponent;
