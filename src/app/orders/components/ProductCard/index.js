import React, { Component } from 'react';
import { View } from 'react-native';

import { TextComponent } from '../../../common';
import styles from '../styles';

class ProductCardComponent extends Component {

  /**
   * @name convertDate 
   * converts date to local time
   * @param {String} - date
   */
  convertDate = (utcdate) => {
    const date = new Date(utcdate);
    const month = date.getMonth()+1; 
    const day = date.getDate();
    const year = date.getFullYear();
    const hour = date.getHours();
    const minutes = date.getMinutes();

    return `Date: ${day}/${month}/${year}    Time: ${hour}:${minutes}`;
  };
  
  render() {
    const { cardContainer, text, orderProduct, date } = this.props;
    return (
      <View style={cardContainer}>
        <TextComponent style={[text, styles.dateText]} text={this.convertDate(date)} />
        {
          orderProduct.map(item => (
            <View style={styles.detailsContainer} key={item.id}>
              <TextComponent style={text} text={`${item.quantity} `} />
              <TextComponent style={text} text={`${item.product.name}`} />
              {item.product.size && <TextComponent style={text} text={`${item.product.size}`} />}
              <TextComponent style={text} text={`${item.product.price}`} />
            </View>
          ))
        }
      </View>
    );
  }
}

export default ProductCardComponent;
