import React, { Component } from 'react';
import { Query, Mutation } from 'react-apollo';
import { ScrollView, ActivityIndicator } from 'react-native';

import {
  TotalCost,
  OrderButtons,
  OrderSummary,
  ProductFields,
  CustomerFields,
  DeliveryFields,
} from '../../components';
import { ErrorComponent } from '../../../common';

import {
  ADD_ORDER,
  GET_ORDERS,
  GET_PRODUCTS_AND_TYPES,
} from '../../duck';

import styles from './styles';
import { createOptions, deliveryTypes } from '../../../constants';
import { validateFormFields, totalCost } from '../../../../services';

class CreateOrderContainer extends Component {
  static navigationOptions = {
    title: 'Add Order',
    headerStyle: {
      ...styles.headerStyle,
      border: 0,
    }
  };

  constructor(props) {
    super(props);
    this.state = {
      quantity: '',
      productId: '',
      delivery: {
        type: '',
        location: null,
        cost: null,
        time: ''
      },
      customer: {
        phoneNumber: '',
        name: '',
        comments: null
      },
      errors: {},
      extrasTitle: 'Select',
      productTitle: 'Select product',
      productTypesTitle: 'Select product type',
      deliveryTypeTitle: 'Select delivery type',
      customerOrders: [],
      isAddProductClicked: false,
      isWalkinSelected: false,
      isPaidDeliverySelected: false,
      totalCost: '',
    };
  }

  /**
   * @name handleChange
   * @description handles text change
   * @param {string} - text entered
   * @param {type} - field type
   * @returns {void}
   */
  handleChange = (type, text) => {
    const { delivery, customer } = this.state;
    switch (type) {
      case 'quantity':
        this.setState({ quantity: text, errors: { quantity: '' } });
        break;
      case 'cost':
        delivery[type] = text.replace(/[^0-9]/g, '');
        this.setState({ errors: { cost: null }, delivery });
        break;
      case 'time':
        this.setState({ errors: { time: '' } });
      case 'location':
        delivery[type] = text;
        this.setState({ delivery, errors: { location: null } });
        break;
      case 'phoneNumber':
        this.setState({ errors: { phoneNumber: '' } });
      case 'name':
      case 'comments':
        customer[type] = text;
        this.setState({ customer });
        break;
      default:
        return null;
    }
  };

  /**
   * @name toggleSelectedOption
   * @description handles selecting of dropdown options
   * @returns {void}
   */
  toggleSelectedOption = (id, key, options) => {
    const { delivery } = this.state;
    let option = options.filter(el => el.id === id)[0];
    option.selected = !option.selected;
    switch (key) {
      case 'productTypes':
        this.setState({ productTypesTitle: option.title });
        break;
      case 'products':
        this.setState({ productTitle: option.title, productId: option.title.id });
        break;
      case 'deliveryTypes':
        delivery['type'] = option.title;
        if (option.title === 'Walk in') {
          this.setState({
            isWalkinSelected: true,
            isPaidDeliverySelected: false
          });
        } else if (option.title === 'Paid delivery') {
          this.setState({
            isWalkinSelected: false,
            isPaidDeliverySelected: true
          });
        } else {
          this.setState({
            isWalkinSelected: false,
            isPaidDeliverySelected: false
          });
        }
        this.setState({
          delivery,
          errors: { type: '' },
          deliveryTypeTitle: option.title
        });
        break;
      default:
        break;
    }
  };

  /**
   * @name filterProductsByProductType
   * @description filters products according to selected product type
   * @returns {Array}
   */
  filterProductsByProductType = (products, productType) => {
    const productsOfSameType = products.filter(
      product => product.product_type.title === productType
    );
    return createOptions(productsOfSameType, 'products');
  };

  /**
   * @name handleAddProduct
   * @description handles adding of order(s) and validation form fields
   */
  handleAddProduct = () => {
    const { productId, quantity } = this.state;
    let errors;
    errors = validateFormFields({ productId, quantity }, ['productId', 'quantity']);
    if (Object.keys(errors).length === 0) {
      this.setState(prevState => ({
        isAddProductClicked: true,
        customerOrders: [
          ...prevState.customerOrders,
          { productId, quantity: parseInt(quantity, 10) }
        ]
      }));
      this.clearClick();
    } else {
      this.setState({ errors });
    }
  };

  /**
   * @name renderValidationError
   * @description sets errors state of each form field
   */
  renderValidationError = field => {
    if (typeof this.state.errors[field] !== 'undefined') {
      return this.state.errors[field];
    }
    return '';
  };

  /**
   * @name clearClick
   * @description resets state to the original shape
   * @returns {void}
   */
  clearClick = () => {
    this.setState({
      quantity: '',
      productId: '',
      productTitle: 'Select product',
      productTypesTitle: 'Select product type',
      extrasTitle: 'Select '
    });
  };

  /**
   * @name clearDeliveryCustomerFields
   * @description Resets state of delivery and customer input fields
   * @returns {void}
   */
  clearDeliveryCustomerFields = () => {
    this.setState({
      delivery: {
        type: '',
        cost: null,
        time: '',
        location: null,
      },
      customer: {
        name: '',
        comments: null,
        phoneNumber: '',
      },
      isWalkinSelected: false,
      isPaidDeliverySelected: false,
      deliveryTypeTitle: 'Select delivery type',
    });
  };

  /**
   * @name saveOrder
   * @description Validates delivery and customer fields and posts customer and delivery data
   * @returns {void}
   */
  saveOrder = mutation => {
    const { delivery, customer, customerOrders } = this.state;
    const { navigation: { navigate } } = this.props;
    let errors = {};
    let deliveryErrs;
    const customerErrs = validateFormFields(customer, ['phoneNumber']);
    if (delivery.type === 'Paid delivery') {
      deliveryErrs = validateFormFields(delivery, ['type', 'cost', 'location']);
    } else {
      deliveryErrs = validateFormFields(delivery, ['type']);
    }
    errors = { ...customerErrs, ...deliveryErrs };
    if (Object.keys(errors).length === 0) {
      mutation({ variables: { customer, delivery, customerOrders } });
      this.clearClick();
      this.clearDeliveryCustomerFields();
      navigate('ViewOrders');
    } else {
      this.setState({ errors });
    }
  };

  /**
   * @name handleDeleteIconClick
   * Removes item/product from customerOrders
   * @param {int} orderProductId
   * @param {int} productId
   */
  handleDeleteIconClick = productId => {
    this.filterOutProductInOrder(productId);
  };

  /**
   * @name filterOutProductInOrder
   * Removes product details from customer orders
   * @param {int} productId
   */
  filterOutProductInOrder = productId => {
    this.setState(prevState => ({
      customerOrders: prevState.customerOrders.filter(
        order => order.productId !== productId
      )
    }));
  };

  render() {
    const {
      extrasTitle,
      productTitle,
      customerOrders,
      isWalkinSelected,
      deliveryTypeTitle,
      productTypesTitle,
      isAddProductClicked,
      isPaidDeliverySelected,
      quantity,
      delivery: { location, cost, time },
      customer: { name, phoneNumber, comments }
    } = this.state;
    return (
      <Query query={GET_PRODUCTS_AND_TYPES}>
        {({ loading, error, data, refetch }) => {
          if (loading)
            return <ActivityIndicator size='large' color='#EF4DB6' />;
          if (error)
            return (
              <ErrorComponent
                message='There was an error loading products.'
                retry={() => refetch()}
              />
            );
          const productTypes = createOptions(data.productTypes);
          const products = this.filterProductsByProductType(
            data.products,
            productTypesTitle
          );
          return (
            <Mutation
              mutation={ADD_ORDER}
              update={(cache, { data: { saveOrder } }) => {
                const { orders } = cache.readQuery({ query: GET_ORDERS });
                console.log('cached order', saveOrder);
                cache.writeQuery({
                  query: GET_ORDERS,
                  data: { orders: [...orders, saveOrder] }
                });
              }}
            >
              {saveOrder => (
                <ScrollView style={styles.container}>
                  <ProductFields
                    quantity={quantity}
                    products={products}
                    productTypes={productTypes}
                    productTitle={productTitle}
                    productTypesTitle={productTypesTitle}
                    handleChange={this.handleChange}
                    toggleSelectedOption={this.toggleSelectedOption}
                    renderValidationError={this.renderValidationError}
                  />
                  <OrderButtons
                    clearText='Clear'
                    saveText='Add product'
                    clearClick={this.clearClick}
                    handleSubmit={this.handleAddProduct}
                  />
                  {isAddProductClicked && customerOrders.length ? (
                    <OrderSummary
                      orders={customerOrders}
                      products={data.products}
                      onDeleteClick={this.handleDeleteIconClick}
                    />
                  ) : null}
                  <DeliveryFields
                    cost={cost && cost.toString()}
                    time={time}
                    location={location}
                    deliveryTypes={deliveryTypes}
                    isWalkinSelected={isWalkinSelected}
                    deliveryTypeTitle={deliveryTypeTitle}
                    handleChange={this.handleChange}
                    isPaidDeliverySelected={isPaidDeliverySelected}
                    toggleSelectedOption={this.toggleSelectedOption}
                    renderValidationError={this.renderValidationError}
                  />
                  {customerOrders.length ? (
                    <TotalCost total={totalCost(customerOrders, cost, data.products)} />
                  ) : null}
                  <CustomerFields
                    name={name}
                    comments={comments}
                    phoneNumber={phoneNumber}
                    renderValidationError={this.renderValidationError}
                    handleChange={this.handleChange}
                  />
                  <OrderButtons
                    clearText='Clear'
                    saveText='Save order'
                    handleSubmit={() => this.saveOrder(saveOrder)}
                    clearClick={this.clearDeliveryCustomerFields}
                  />
                </ScrollView>
              )}
            </Mutation>
          );
        }}
      </Query>
    );
  }
}

export default CreateOrderContainer;
