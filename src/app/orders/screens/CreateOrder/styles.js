import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  headerStyle: {
    backgroundColor: '#FFF5EE',
  },
  container: {
    backgroundColor: '#FFF5EE',
    paddingTop: 20,
    paddingLeft: 10,
    paddingRight: 10,
    flex: 1
  }
});

export default styles;
