import React, { Component } from 'react';
import { Query, Mutation } from 'react-apollo';
import { ScrollView, ActivityIndicator, View } from 'react-native';

import {
  ErrorComponent,
  TextComponent,
} from '../../../common';
import {
  ProductFields,
  DeliveryFields,
  TotalCost,
  OrderButtons,
  CustomerFields,
  EditDeleteButtons,
} from '../../components'

import styles from './styles';
import { stripTypenames } from '../../helpers'
import { DELIVERY_TYPES } from '../../constants'
import { createOptions, deliveryTypes } from '../../../constants';
import { GET_PRODUCTS_AND_TYPES, UPDATE_ORDER } from '../../duck';
import { validateFormFields } from '../../../../services';

class EditOrderContainer extends Component {
  static navigationOptions = {
    title: 'Edit Order',
    headerStyle: {
      backgroundColor: '#FFF5EE',
      border: 0
    }
  };

  productFieldState = {
    orderId: '',
    productId: '',
    quantity: null,
    product: '',
    productTitle: 'Select product',
    productTypesTitle: 'Select product type',
  };

  deliveryCustomerFields = {
    delivery: {
      type: '',
      cost: null,
      time: '',
      location: null,
    },
    customer: {
      name: '',
      comments: null,
      phoneNumber: '',
    },
    isWalkinSelected: false,
    isPaidDeliverySelected: false,
    deliveryTypeTitle: 'Select delivery type',
  }

  constructor(props) {
    super(props);
    const { order } = props.navigation.state.params;
    this.state = {
      customerOrder: order.orderProduct,
      isProductFormOpen: false,
      selectedProduct: {},
      errors: {},
      ...this.productFieldState,
      ...this.deliveryCustomerFields,
      customer: {
        ...this.deliveryCustomerFields.customer,
        ...order.customer
      },
      delivery: {
        ...this.deliveryCustomerFields.delivery,
        ...order.delivery,
      },
      ...this.getWalkinPaidDeliveryState(order.delivery.type),
      deliveryTypeTitle: order.delivery.type,
    }
  }

  getWalkinPaidDeliveryState = (deliverType) => {
    switch(deliverType) {
      case DELIVERY_TYPES.WALKIN:
        return {
          isWalkinSelected: true,
          isPaidDeliverySelected: false
        }
      case DELIVERY_TYPES.PAID:
        return {
          isWalkinSelected: false,
          isPaidDeliverySelected: true
        }
      default:
        return {
          isWalkinSelected: false,
          isPaidDeliverySelected: false
        }
    }
  }

  /**
   * @name toggleSelectedOption
   * @description handles selecting of dropdown options
   * @returns {void}
   */
  toggleSelectedOption = (id, key, options) => {
    const delivery = {};
    let option = options.filter( el => el.id === id)[0];
    option.selected = !option.selected;
    switch(key) {
      case 'productTypes':
        this.setState({productTypesTitle: option.title });
        break;
      case 'products':
        // order['productId'] = option.title.id;
        this.setState({ productTitle: option.title, product: option.title });
        break;
      case 'deliveryTypes':
        delivery['type'] = option.title;
        this.setState({
          delivery: { ...this.state.delivery, ...delivery },
          errors:{ type: '' },
          deliveryTypeTitle: option.title,
          ...this.getWalkinPaidDeliveryState(option.title)
        });
        break;
      default:
        break
    }
    this.setState({ [key]: option });
  }

  /**
   * @name handleChange
   * @description handles text change
   * @param {string} - text entered
   * @param {type} - field type
   * @returns {void}
   */
  handleChange = (type, text) => {
    const { delivery, customer } = this.state;
    switch (type) {
      case 'quantity':
        this.setState({ quantity: text, errors: { quantity: '' } });
        break;
      case 'cost':
        delivery[type] = text.replace(/[^0-9]/g, '');
        this.setState({ errors: { cost: null }, delivery });
        break;
      case 'time':
        this.setState({ errors: { time: '' } });
      case 'location':
        delivery[type] = text;
        this.setState({ delivery, errors: { location: null } });
        break;
      case 'phoneNumber':
        this.setState({ errors: { phoneNumber: '' } });
      case 'name':
      case 'comments':
        customer[type] = text;
        this.setState({ customer });
        break;
      default:
        return null;
    }
  };

  /**
   * @name clearClick
   * @description resets product fields state to the original shape
   * @returns {void}
   */
  clearClick = () => {
    this.setState({
      ...this.productFieldState,
    });
  }

  clearDeliveryCustomerFields = () => {
    this.setState({
      ...this.deliveryCustomerFields,
    });
  }

  /**
   * @name handleEditProductClick
   * Handles edit icon click on a product
   * @param {object} product
   */
  handleEditProductClick = (orderId, product, quantity) => {
    this.setState({ 
      orderId,
      quantity,
      productId: product.id,
      product: product,
      productTitle: product.name,
      productTypesTitle: product.product_type.title,
      isProductFormOpen: true
    });
  }

  /**
   * @name renderValidationError
   * @description sets errors state of each form field
   */
  renderValidationError = (field) => {
    const { errors } = this.state;
    if (typeof errors[field] !== 'undefined') {
      return errors[field];
    }
    return '';
  }

  /**
   * @name filterProductsByProductType
   * @description filters products according to selected product type
   * @returns {Array}
   */
  filterProductsByProductType = (products, productType) => {
    const productsOfSameType = products.filter( product => product.product_type.title === productType);
    return createOptions(productsOfSameType, 'products');
  }

  /**
   * @name filterOutProductInOrder
   * Removes product details from customer orders
   * @param {int} productId
   */
  filterOutProductInOrder = (orderId, productId) => {
    this.setState(prevState => ({
      customerOrder: prevState.customerOrder.filter(
        custOrder => custOrder.id === orderId && custOrder.product.id !== productId
      )
    }));
  };

  /**
   * @name handleDeleteIconClick
   * Removes item/product from customerOrders
   * @param {int} orderProductId
   * @param {int} productId
   */
  handleDeleteClick = (orderId, productId) => {
    this.filterOutProductInOrder(orderId, productId);
  };

  /**
   * @name handleUpdateProduct
   * @description handles updating of order(s) and validation form fields
   */
  handleUpdateProduct = () => {
    const { quantity, product, orderId, customerOrder, productTitle } = this.state;

    let errors;
    errors = validateFormFields({ productTitle, quantity }, ['productTitle', 'quantity']);

    if (Object.keys(errors).length === 0) {
      const updatedOrder = customerOrder.map(order => {
        if (order.id === orderId) {
          return {
            ...order,
            quantity: parseInt(quantity, 10),
            product,
          }
        }
        return order;
      });
      this.setState({ customerOrder: updatedOrder });
      this.clearClick();
    } else {
      this.setState({ errors });
    }
  }

  /**
   * @name updateOrder
   * @description Validates and submits form data
   * @returns {void}
   */
  updateOrder = mutation => {
    const { delivery, customer, customerOrder } = this.state;
    const { navigation: { navigate } } = this.props;

    let errors = {};
    let deliveryErrs;
    const customerErrs = validateFormFields(customer, ['phoneNumber']);
    if (delivery.type === DELIVERY_TYPES.PAID) {
      deliveryErrs = validateFormFields(delivery, ['type', 'cost', 'location']);
    } else {
      deliveryErrs = validateFormFields(delivery, ['type']);
    }
    errors = { ...customerErrs, ...deliveryErrs };
    if (Object.keys(errors).length === 0) {
      mutation({ 
        variables: {
          customer: stripTypenames(customer),
          delivery: stripTypenames(delivery),
          customerOrders: customerOrder.map(order =>
              ({
                  id: order.id,
                  quantity: order.quantity,
                  productId: order.product.id,
              })
            )
        },
      });
      this.clearClick();
      this.clearDeliveryCustomerFields();
      navigate('ViewOrders');
    } else {
      this.setState({ errors });
    }
  }
  
  renderProductsInOrder = (data) => {
    const {
      productId,
      quantity,
      productTitle,
      productTypesTitle,
      isProductFormOpen,
      customerOrder
    } = this.state;

    return customerOrder.map( order => {
      const { id, product, quantity: productQuantity } = order;
      return (isProductFormOpen && productId === product.id) ? (
        <View key={id}>
          <ProductFields
            quantity={quantity.toString()}
            productTypes={createOptions(data.productTypes)}
            productTitle={productTitle}
            productTypesTitle={productTypesTitle}
            products={this.filterProductsByProductType(data.products, productTypesTitle)}
            handleChange={this.handleChange}
            toggleSelectedOption={this.toggleSelectedOption}
            renderValidationError={this.renderValidationError}
          />
          <OrderButtons
            clearText='Cancel'
            saveText='Save'
            clearClick={this.clearClick}
            handleSubmit={this.handleUpdateProduct}
          />
        </View>
      ): (
        <View style={styles.textContainer} key={id}>
          {productQuantity && (<TextComponent style={styles.text} text={productQuantity} />)}
          {product.name && (
            <TextComponent style={styles.text} text={product.name} />
          )}
          {product.size && (
            <TextComponent style={styles.text} text={product.size} />
          )}
          {product.price && (
            <TextComponent style={styles.text} text={product.price} />
          )}
          <EditDeleteButtons
            id={id}
            product={product}
            quantity={productQuantity}
            handleDeleteClick={this.handleDeleteClick}
            handleEditProductClick={this.handleEditProductClick}
          />
        </View>
      )
    });
  }

  render() {
    const {
      customerOrder,
      isWalkinSelected,
      deliveryTypeTitle,
      isPaidDeliverySelected,
      delivery: { location, cost, time },
      customer: { name, phoneNumber, comments },
    } = this.state;

    return(
      <Query query={GET_PRODUCTS_AND_TYPES}>
        {
          ({ loading, error, data, refetch }) => {
            if (loading)
              return <ActivityIndicator size='large' color='#EF4DB6' />;
            if (error)
              return (
                <ErrorComponent
                  message='There was an error loading products.'
                  retry={() => refetch()}
                />
              );
              return (
                <Mutation mutation={UPDATE_ORDER}>
                { updateOrder => (
                    <ScrollView style={styles.container}>
                      {this.renderProductsInOrder(data)}
                      <DeliveryFields
                        cost={cost && cost.toString()}
                        time={time}
                        location={location}
                        deliveryTypes={deliveryTypes}
                        isWalkinSelected={isWalkinSelected}
                        deliveryTypeTitle={deliveryTypeTitle}
                        handleChange={this.handleChange}
                        isPaidDeliverySelected={isPaidDeliverySelected}
                        toggleSelectedOption={this.toggleSelectedOption}
                        renderValidationError={this.renderValidationError}
                      />
                      <TotalCost total={totalCost(customerOrder, cost, data.products)} />
                      <CustomerFields
                        name={name}
                        comments={comments}
                        phoneNumber={phoneNumber}
                        renderValidationError={this.renderValidationError}
                        handleChange={this.handleChange}
                      />
                      <OrderButtons
                        clearText='Clear'
                        saveText='Save order'
                        handleSubmit={() => this.updateOrder(updateOrder)}
                        clearClick={this.clearDeliveryCustomerFields}
                      />
                    </ScrollView>
                  )
                }
                </Mutation>
              )
          }
        }
      </Query>
    )
  }
}

export default EditOrderContainer;
