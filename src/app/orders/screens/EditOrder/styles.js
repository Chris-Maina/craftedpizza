import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#FFF5EE',
    paddingTop: 10,
    paddingLeft: 10,
    paddingRight: 10,
    flex: 1
  },
  textContainer: {
    padding: 10,
    backgroundColor: '#ffffff',
    shadowOpacity: 0.2,
    borderWidth: 0,
    borderRadius: 2,
    marginBottom: 20,
    shadowOffset: { width: 2, height: 2 },
    flexDirection: 'row',
  },
  text: {
    textAlign: 'left',
    marginLeft: 5
  },
  buttonActions: {
    marginLeft: 'auto',
    flexDirection: 'row',
  }
});

export default styles;
