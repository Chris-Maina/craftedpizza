import React, { Component } from 'react';
import { Mutation } from 'react-apollo';
import { View } from 'react-native';

import {
  IconComponent,
  TextComponent,
  ButtonComponent,
  ListItemComponent
} from '../../../common';
import { ProductCard, DeliveryCard, CustomerCard } from '../../components';
import { GET_ORDERS, DELETE_ORDER } from '../../duck/orders.query';
import styles from './styles';

class SingleOrderContainer extends Component { 

  static navigationOptions = {
    title: 'Details',
    headerStyle: {
      ...styles.headerStyle,
      border: 0,
    },
  }

  /**
   * @name handleDeleteOrder
   * Deletes an order
   * @param {func} mutation
   * @param {int} id
   */
  handleDeleteOrder = (mutation, id) => {
    const { navigation: { navigate } } = this.props;
    mutation({ variables: { id, }});
    navigate('ViewOrders');
  }

  render () {
  const { navigation: { navigate, state }} = this.props;
  const { order } = state.params;
    return (
      <Mutation
        mutation={DELETE_ORDER}
        update={
          (cache) => {
            const { orders } = cache.readQuery({ query: GET_ORDERS });
            cache.writeQuery({
              query: GET_ORDERS,
              data: { orders: orders.filter(el => el.id !== order.id)}
            });
          }
        }
      >
        {
          deleteOrder => (
            <View style={styles.container}>
              <ListItemComponent style={styles.card}>
                <View style={styles.headerContainer}>
                  <TextComponent style={styles.header} text={order.id}/>
                  <ButtonComponent
                    style={styles.deleteButton}
                    onPress={() => this.handleDeleteOrder(deleteOrder, order.id)}
                  >
                    <IconComponent
                      name='delete'
                      size={30}
                      color='white'
                      style={styles.icon}
                    />
                  </ButtonComponent>
                </View>
                <ProductCard
                  text={styles.text}
                  date={order.createdAt}
                  orderProduct={order.orderProduct}
                />
                {
                  order.delivery.location &&
                    <DeliveryCard
                      text={styles.text}
                      delivery={order.delivery}
                    />
                }
                <CustomerCard
                  text={styles.text}
                  customer={order.customer}
                />
              </ListItemComponent>
              <ButtonComponent
                onPress={() => navigate('EditOrder', { order })}
                style={styles.button}>
                <IconComponent
                  name='edit'
                  size={40}
                  color='white'
                  style={styles.icon}
                  />
              </ButtonComponent>
            </View>
          )
        }
      </Mutation>
    )
  }
}

export default SingleOrderContainer;
