import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  headerStyle: {
    backgroundColor: '#FFF5EE',
  },
  container: {
    flex: 1,
    paddingTop: 20,
    paddingLeft: 10,
    paddingRight: 10,
    backgroundColor: '#FFF5EE'
  },
  card: {
    flexDirection: 'column',
    padding: 20,
    height: 300,
    backgroundColor: '#F9FBFD'
  },
  headerContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingBottom: 10,
    borderBottomColor: '#A2A2A2',
    borderBottomWidth: 0.5,
  },
  header: {
    fontSize: 24,
    fontWeight: '400'
  },
  deleteButton: {
    borderRadius: 62.1,
    width: 40,
    height: 40,
    backgroundColor: '#BF0F0F'
  },
  text: {
    fontSize: 18,
    marginTop: 3,
    marginLeft: 5,
  },
  button: {
    position: 'absolute',
    top: 280,
    right: 20,
    borderRadius: 62.1,
    width: 62.1,
    height: 62.1,
    backgroundColor: '#09A494',
    zIndex: 1,
  },
  icon: {
    alignSelf: 'center',
    marginTop: 'auto',
    marginBottom: 'auto',
    zIndex: 5,
  }
})

export default styles;
