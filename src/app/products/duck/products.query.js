import gql from "graphql-tag";
import { PRODUCT_TYPE_FRAGMENT } from "../../productTypes/duck";

export const PRODUCT_FRAGMENT = gql`
  fragment Product on Product {
    id
    name
    price
    size
  }
`;

export const GET_PRODUCTS = gql`
  query getProducts {
    products {
      ...Product
      product_type {
        ...ProductType
      }
    }
    productTypes {
      ...ProductType
    }
  }
  ${PRODUCT_FRAGMENT}
  ${PRODUCT_TYPE_FRAGMENT}
`;

export const ADD_PRODUCT = gql`
  mutation createProduct(
    $name: String!
    $size: String
    $price: Int!
    $productTypeId: Int!
  ) {
    createProduct(
      name: $name
      size: $size
      price: $price
      productTypeId: $productTypeId
    ) {
      ...Product
      product_type {
        ...ProductType
      }
    }
  }
  ${PRODUCT_FRAGMENT}
  ${PRODUCT_TYPE_FRAGMENT}
`;

export const DELETE_PRODUCT = gql`
  mutation deleteProduct($id: Int) {
    deleteProduct(id: $id)
  }
`;

export const UPDATE_PRODUCT = gql`
  mutation updateProduct($id: Int, $name: String, $size: String, $price: Int) {
    updateProduct(id: $id, name: $name, size: $size, price: $price) {
      ...Product
      product_type {
        ...ProductType
      }
    }
  }
  ${PRODUCT_FRAGMENT}
  ${PRODUCT_TYPE_FRAGMENT}
`;
