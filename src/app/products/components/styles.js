import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  label: {
    fontSize: 18,
    marginBottom: 5,
    color: '#442C2E',
  },
  inputContainer: {
    marginBottom: 20
  },
  errorMessage: {
    color: "red",
    textTransform: 'capitalize'
  },
});

export default styles;
