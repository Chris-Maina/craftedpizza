import React, { Component } from 'react';
import { View } from 'react-native';

import styles from '../styles';
import { TextInputComponent, TextComponent, DropdownComponent } from '../../../common'

class PizzaFields extends Component {
  renderInputField = (name, placeholder, onChange, value, renderValidationError) => {
    return (
      <View style={styles.inputContainer}>
        <TextComponent style={styles.label} text={name} />
        <TextInputComponent
          onChangeText={text => onChange(name.toLowerCase(), text)}
          placeholder={placeholder}
          value={value}
          name={name.toLowerCase()}
        />
        <TextComponent
          // text={renderValidationError(name.toLowerCase())}
          style={styles.errorMessage}
        />
      </View>
    );
  }
  renderDropdownField = (name, options, value, toggleItem, renderValidationError) => {
    return (
      <View style={styles.inputContainer}>
        <TextComponent style={styles.label} text={name} />
        <DropdownComponent
          options={options}
          title={value}
          toggleItem={toggleItem}
        />
        <TextComponent
          // text={renderValidationError(name.toLowerCase())}
          style={styles.errorMessage}
        />
      </View>
    )
  } 
  render(){
    const { 
      name,
      price,
      sizes,
      title,
      handleChange,
      handleNameChange,
      handlePriceChange,
      toggleSelectedSize,
      renderValidationError,
    } = this.props;
    return (
      <View>
        {this.renderInputField('Name', 'Enter product name', handleChange, name, renderValidationError)}
        {this.renderDropdownField('Size', sizes, title, toggleSelectedSize, renderValidationError)}
        {this.renderInputField('Price', 'Price', handleChange, price.toString(), renderValidationError)}
      </View>
    )
  }
}

export default PizzaFields;
