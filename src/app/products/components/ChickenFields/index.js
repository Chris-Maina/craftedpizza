import React, { Component } from 'react';
import { View } from 'react-native';

import styles from '../styles';
import { TextInputComponent, TextComponent } from '../../../common'

class ChickenFields extends Component {
  renderInputField = (name, placeholder, onChange, value, renderValidationError) => {
    return (
      <View style={styles.inputContainer}>
        <TextComponent style={styles.label} text={name} />
        <TextInputComponent
          value={name}
          onChangeText={text => onChange(name.toLowerCase(), text)}
          placeholder={placeholder}
          value={value}
        />
        <TextComponent
          text={renderValidationError(name.toLowerCase())}
          style={styles.errorMessage}
        />
      </View>
    );
  }
  render() {
    const { 
      name,
      price,
      handleChange,
      handleNameChange,
      handlePriceChange,
      renderValidationError,
    } = this.props;
    return (
      <View>
        {this.renderInputField('Name', 'Enter product name', handleChange, name, renderValidationError)}
        {this.renderInputField('Price', 'Price per kg', handleChange, price.toString(), renderValidationError)}
      </View>
    )
  }
}

export default ChickenFields;
