import React, { Component } from 'react';
import { Mutation } from 'react-apollo';

import { FormComponent } from '../common';

import { sizes } from '../constants';
import { UPDATE_PRODUCT, GET_PRODUCTS } from './duck/products.query';
import createField from '../../services/createField';
import validateFormFields from '../../services/formValidator';

export class EditProductContainer extends Component {

  static navigationOptions({ navigation }) {
    const { title } = navigation.state.params.item.product_type;
    return {
      title: `Edit ${title}`,
    }
  }

  constructor(props) {
    super(props);
    const { item } = props.navigation.state.params;
    this.state = {
      title: 'Select',
      sizes: sizes,
      product: { ...item },
      errors: {}
    }
  }

  /**
   * @name handleNameChange
   * @description on change text handler for name input
   * @param {name} - name of the product
   */
  handleNameChange = name => {
    const { product } = this.state;
    product['name'] = name;
    this.setState({ product });
  }

  /**
   * @name handlePriceChange
   * @description on change text handler for price input
   * @param {price} - price of the product
   */
  handlePriceChange = price => {
    const { product } = this.state;
    product['price'] = parseInt(price);
    this.setState({ product });
  }

  toggleSelectedSize = (id, key) => {
    const { product } = this.state;
    let tempSizes = this.state[key];
    tempSizes[id].selected = !tempSizes[id].selected;
    product['size'] = tempSizes[id].title
    this.setState({ [key]: tempSizes, product });
  }

  /**
   * @name renderValidationError
   * @description displays error of the field provided if any
   * @param {field} - input field
   */
  renderValidationError = field => {
    if (typeof this.state.errors[field] !== "undefined") {
      return this.state.errors[field];
    }
    return null;
  }

  /**
   * @name cancelClick
   * @description empties form fields
   */
  cancelClick = () => {
    const { productType } = this.props.navigation.state.params.item
    this.setState({
      product: {
        name: '',
        price: '',
        size: '',
        productType
      },
    })
  }

  /**
   * @name validate
   * @description validates the submitted data i.e product
   * @param {Object} - product - submitted product
   * @param {string} - type - product type
   */
  validate = (product) => {
    const { title } = product.product_type;
    switch(title) {
      case 'pizza':
        return validateFormFields(product, ['name', 'size', 'price']);
      default:
        return validateFormFields(product, ['name', 'price']);
    }
  }

  /**
   * @name handleSubmit
   * @description on submit handler for the product
   * @param {func} mutation 
   */
  handleSubmit = (mutation) => {
    const { product } = this.state;
    const { navigation: { navigate } } = this.props;
    const errors = this.validate(product);
    if (Object.keys(errors).length === 0) {
      mutation({ variables: { ...product }});
      navigate('Prices');
    } else {
      this.setState({ errors });
    }
  }

  /**
   * @name getFieldsToRender
   * @description returns form fields based on product type
   * @param {string} - product type 
   */
  getFieldsToRender = (product) => {
    const { sizes, title } = this.state;
    const { name, price, size, product_type } = product;
    let fields;
    switch (product_type.title.toLowerCase()) {
      case 'chicken':
        fields = [
          createField('Name', 'input', 'Enter product name', name, null, this.handleNameChange, this.renderValidationError),
          createField('Price', 'input', 'Price per kg', price.toString(), null, this.handlePriceChange, this.renderValidationError),
        ];
        return fields;
      case 'fries': 
        fields = [
          createField('Name', 'input', 'Enter product name', name, null, this.handleNameChange, this.renderValidationError),
          createField('Price', 'input', 'Price', price.toString(), null, this.handlePriceChange, this.renderValidationError),
        ]
        return fields;
      case 'pizza':
        fields = [
          createField('Name', 'input', 'Enter product name', name, null, this.handleNameChange, this.renderValidationError),
          createField('Size', 'dropdown', 'Name', size || title, sizes, this.toggleSelectedSize, this.renderValidationError),
          createField('Price', 'input', 'Price', price.toString(), null, this.handlePriceChange, this.renderValidationError), 
        ];
        return fields;
      default:
        return fields;
    }
  }

  /**
   * @name getUpdatedProducts
   * updates products with the new product
   * @param {Array} products
   * @param {Object} newProduct
   */
  getUpdatedProducts = (products, newProduct) => (
    products.map(prod => (
      prod.id === newProduct.id ? newProduct : prod
    ))
  )
  
  render () {
    const { product } = this.state;
    return (
      <Mutation 
      mutation={UPDATE_PRODUCT}
      update={
        (cache, { data: updateProduct }) => {
          const { products } = cache.readQuery({ query: GET_PRODUCTS });
          cache.writeQuery({
            query: GET_PRODUCTS,
            data: { products: this.getUpdatedProducts(products, updateProduct) }
          });
        }
      }
      >
      {
        (updateProduct) => (
          <FormComponent
            fields={this.getFieldsToRender(product)}
            iconName='check'
            cancelText='Clear'
            cancelClick={this.cancelClick}
            handleSubmit={() => this.handleSubmit(updateProduct)}
          />
        )
      }
      </Mutation>
    );
  }
}
