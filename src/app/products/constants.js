const PRODUCT_TYPES = {
  1: 'chicken',
  2: 'fries',
  3: 'pizza'
};

export { PRODUCT_TYPES };
