export * from './ProductsComponent';
export * from './EditProductContainer';

import ProductScreen from './Container';

export * from './screens';
export default ProductScreen;
