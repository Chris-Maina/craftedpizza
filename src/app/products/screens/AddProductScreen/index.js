import React, { Component } from "react";
import { Mutation } from 'react-apollo';
import { View } from "react-native";

import { PizzaFields, FriesFields, ChickenFields } from "../../components";
import { TextComponent, ButtonComponent, IconComponent } from "../../../common";

import styles from "./styles";
import { sizes } from "../../../constants";
import { PRODUCT_TYPES } from "../../constants";
import { ADD_PRODUCT, GET_PRODUCTS } from '../../duck';
import validateFormFields from "../../../../services/formValidator";

class AddProduct extends Component {
  static navigationOptions = {
    title: "Add Product"
  };

  constructor(props) {
    super(props);
    const { params } = props.navigation.state;
    this.state = {
      title: "Sizes",
      product: {
        name: "",
        price: "",
        size: null,
        productType: params.productType ? params.productType : ""
      },
      sizes,
      errors: {}
    };
  }

  handleChange = (type, name) => {
    let product = {};
    if (type === "price") {
      product[type] = parseInt(name);
    } else {
      product[type] = name;
    }
    this.setState(prevState => ({
      product: { ...prevState.product, ...product }
    }));
  };

  /**
   * @name handleNameChange
   * @description on change text handler for name input
   * @param {name} - name of the product
   */
  handleNameChange = name => {
    const { product } = this.state;
    product["name"] = name;
    this.setState({ product });
  };

  /**
   * @name handlePriceChange
   * @description on change text handler for price input
   * @param {price} - price of the product
   */
  handlePriceChange = price => {
    const { product } = this.state;
    product["price"] = parseInt(price);
    this.setState({ product });
  };

  toggleSelectedSize = (id, key) => {
    const { product } = this.state;
    let tempSizes = this.state[key];
    tempSizes[id].selected = !tempSizes[id].selected;
    product["size"] = tempSizes[id].title;
    this.setState({ [key]: tempSizes, title: tempSizes[id].title, product });
  };

  /**
   * @name renderValidationError
   * @description displays error of the field provided if any
   * @param {field} - input field
   */
  renderValidationError = field => {
    if (typeof this.state.errors[field] !== "undefined") {
      return this.state.errors[field];
    }
    return null;
  };

  /**
   * @name cancelClick
   * @description empties form fields
   */
  cancelClick = () => {
    const { productType } = this.props.navigation.state.params;
    this.setState({
      product: {
        name: "",
        price: "",
        size: null,
        productType
      }
    });
  };

  /**
   * @name validate
   * @description validates the submitted data i.e product
   * @param {Object} - product - submitted product
   * @param {string} - type - product type
   */
  validate = (product, type) => {
    switch (type) {
      case "pizza":
        return validateFormFields(product, ["name", "size", "price"]);
      default:
        return validateFormFields(product, ["name", "price"]);
    }
  };

  /**
   * @name handleSubmit
   * @description on submit handler for the product
   */
  handleSubmit = mutation => {
    const { product } = this.state;
    const {
      navigation: {
        navigate,
      }
    } = this.props;

    const errors = this.validate(product, product.productType.title);
    if (Object.keys(errors).length === 0) {
      mutation({ variables: { ...product, productTypeId: product.productType.id } });
      navigate("Prices");
    } else {
      this.setState({ errors });
    }
  };

  renderProductFields = type => {
    const {
      product: { name, price },
    } = this.state;

    switch (type.toLowerCase()) {
      case PRODUCT_TYPES[1]:
        return (
          <ChickenFields
            name={name}
            price={price}
            handleChange={this.handleChange}
            handleNameChange={this.handleNameChange}
            handlePriceChange={this.handlePriceChange}
            renderValidationError={this.renderValidationError}
          />
        );
      case PRODUCT_TYPES[2]:
        return (
          <FriesFields
            name={name}
            price={price}
            handleChange={this.handleChange}
            handleNameChange={this.handleNameChange}
            handlePriceChange={this.handlePriceChange}
            renderValidationError={this.renderValidationError}
          />
        );
      default:
        return (
          <PizzaFields
            sizes={sizes}
            name={name}
            price={price}
            handleChange={this.handleChange}
            handleNameChange={this.handleNameChange}
            handlePriceChange={this.handlePriceChange}
            toggleSelectedSize={this.toggleSelectedSize}
            renderValidationError={this.renderValidationError}
          />
        );
    }
  };

  // renderDropdownInput = (name) => {
  //   const { toggleSelectedOption, renderValidationError } = this.props;
  //   return (
  //     <View style={styles.inputContainer}>
  //       <DropdownComponent
  //         options={options}
  //         title='Select a category'
  //         toggleItem={toggleSelectedOption}
  //       />
  //       <TextComponent
  //         text={renderValidationError(name)}
  //         style={styles.errorMessage}
  //       />
  //     </View>
  //   );
  // };

  render() {
    const { productType } = this.props.navigation.state.params;

    return (
      <Mutation
        mutation={ADD_PRODUCT}
        update={
          (cache, { data: { createProduct }}) => {
            const { products } = cache.readQuery({ query: GET_PRODUCTS });
            console.log('product >>>', createProduct)
            cache.writeQuery({
              query: GET_PRODUCTS,
              data: { products: [...products, createProduct]}
            });
          }
        }
      >
        { 
          createProduct => (
            <View style={styles.container}>
              
              {this.renderProductFields(productType.title.toLowerCase())}
              <View style={styles.buttonContainer}>
                <ButtonComponent style={styles.cancelButton} onPress={this.cancelClick}>
                  <TextComponent text="Cancel" style={styles.cancelText} />
                </ButtonComponent>
                <ButtonComponent
                  style={styles.button}
                  onPress={() => this.handleSubmit(createProduct)}
                >
                  <IconComponent
                    name="check"
                    size={40}
                    color="white"
                    style={styles.icon}
                  />
                </ButtonComponent>
              </View>
            </View>
          )
        }
      </Mutation>
    );
  }
}

export default AddProduct;
