import { StyleSheet } from "react-native";

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 10,
    backgroundColor: '#FFF5EE'
  },
  buttonContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between'
  },
  button: {
    borderRadius: 62.1,
    width: 62.1,
    height: 62.1,
    backgroundColor: '#09A494',
  },
  icon: {
    alignSelf: 'center',
    marginTop: 'auto',
    marginBottom: 'auto',
  },
  cancelButton: {
    borderRadius: 8,
    width: 115.6,
    height: 40,
    backgroundColor: '#FFFFFF',
    
  },
  cancelText: {
    alignSelf: 'center',
    marginTop: 'auto',
    marginBottom: 'auto',
    color: '#442C2E',
    fontSize: 18
  }
});

export default styles;
