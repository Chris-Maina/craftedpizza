import AddProductScreen from './AddProductScreen';
import SingleProductScreen from './SingleProductScreen';

export { AddProductScreen, SingleProductScreen };