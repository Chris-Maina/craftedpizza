import React, { Component } from 'react';
import { View } from 'react-native';

import WithSingleProduct from '../../WithSingleProduct';
import {
  ListItemComponent,
  TextComponent,
  ButtonComponent,
  IconComponent
 } from '../../../common';

 import styles from './styles';
 import { capitalize } from '../../../../services/capitalize';

class SingleProductScreen extends Component{
  static navigationOptions = {
    title: 'Details',
    headerStyle: {
      backgroundColor: '#FFF5EE',
      border: 0
    },
  }

  renderLabelAndText = (labelText, value) => {
    if(value) return (
      <View style={styles.textContainer}>
        <TextComponent style={styles.labelText} text={labelText}/>
        <TextComponent style={styles.text}  text={value} />
      </View>
    );
  };
  render() {
    const { navigation } = this.props;
    return (
      <WithSingleProduct
        navigation={navigation}
        render={
        (item, handleDeleteClick, handleEditClick) => (
          <View style={styles.container}>
            <ListItemComponent style={styles.card}>
              <View style={styles.headerContainer}>
                <TextComponent style={styles.header} text={capitalize(item.product_type.title)}/>
                <ButtonComponent style={styles.deleteButton} onPress={() => handleDeleteClick(item.id)}>
                  <IconComponent
                    name='delete'
                    size={30}
                    color='white'
                    style={styles.icon}
                  />
                </ButtonComponent>
              </View>
              <View>
                {
                  this.renderLabelAndText('Name', item.name)
                }
                {
                  this.renderLabelAndText('Size', item.size)
                }
                {
                  this.renderLabelAndText('Price', item.price)
                }
              </View>
            </ListItemComponent>
            <ButtonComponent style={styles.button} onPress={handleEditClick}>
              <IconComponent
                name='edit'
                size={40}
                color='white'
                style={styles.icon}
              />
            </ButtonComponent>
          </View>
        )
      } />
    );
  }
}

export default SingleProductScreen;
