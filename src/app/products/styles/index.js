import { StyleSheet } from "react-native";

const styles = StyleSheet.create({
  container: {
    backgroundColor: "#FFF5EE",
    paddingTop: 10,
    paddingLeft: 10,
    paddingRight: 10,
    flex: 1
  },
  categoryText: {
    fontSize: 16,
    paddingLeft: 3,
    paddingBottom: 8,
    color: "#442C2E"
  },
  card: {
    height: 47
  },
  cardText: {
    fontSize: 14,
    color: "#442C2E",
    alignSelf: "center"
  },
  messageContainer: {
    marginTop: "auto",
    marginBottom: "auto"
  },
  messageText: {
    fontSize: 16,
    fontWeight: "500",
    color: "#442C2E",
    textAlign: "center"
  },
  addButton: {
    alignSelf: "flex-end",
    position: "absolute",
    zIndex: 1,
    bottom: 28,
    right: 10,
    borderRadius: 62.1,
    width: 62.1,
    height: 62.1,
    backgroundColor: "#09A494"
  },
  icon: {
    alignSelf: "center",
    marginTop: "auto",
    marginBottom: "auto"
  }
});

export default styles;