import React, { Component } from "react";
import { StyleSheet, SectionList, View } from "react-native";
import { Query } from "react-apollo";
import { ActivityIndicator } from "react-native";

import {  } from "../common";

import { GET_PRODUCTS } from "./duck/products.query";
import {
  ButtonComponent,
  IconComponent,
  TextComponent,
  ErrorComponent,
  ListItemComponent,
} from "../common";

export class ProductsComponent extends Component {
  static navigationOptions = {
    title: "Price list",
    headerStyle: {
      backgroundColor: "#FFF5EE",
      border: 0
    }
  };

  renderItem = item => {
    const {
      navigation: { navigate }
    } = this.props;
    return (
      <ListItemComponent
        key={item.id}
        style={styles.card}
        onPress={() => navigate("SingleProduct", { item })}
      >
        <TextComponent text={item.name} style={styles.cardText} />
        {item.size && (
          <TextComponent text={item.size} style={styles.cardText} />
        )}
        {item.weight && (
          <TextComponent text={item.weight} style={styles.cardText} />
        )}
        <TextComponent text={item.price} style={styles.cardText} />
      </ListItemComponent>
    );
  };

  /**
   * @name getSections
   * Helper method to generate sections from product types
   * @param {Array} - products
   * @param {Array} - productTypes
   * @returns {Array} - sections
   */
  getSections = (products, productTypes) => {
    const sections = [];
    for (type of productTypes) {
      const data = products.filter(
        item => item.product_type.title === type.title
      );
      if (!data.length) continue;
      sections.push({ title: type.title, data });
    }
    return sections;
  };

  render() {
    const {
      navigation: { navigate }
    } = this.props;
    return (
      <Query query={GET_PRODUCTS}>
        {({ loading, error, data, refetch }) => {
          if (loading)
            return <ActivityIndicator size="large" color="#EF4DB6" />;
          if (error)
            return (
              <ErrorComponent
                message="There was an error loading products"
                retry={() => refetch()}
              />
            );
          const { products, productTypes } = data;
          return (
            <View style={styles.container}>
              {products.length ? (
                <SectionList
                  keyExtractor={item => item.id}
                  renderSectionHeader={({ section }) => (
                    <TextComponent
                      text={section.title}
                      style={styles.categoryText}
                    />
                  )}
                  renderItem={({ item }) => this.renderItem(item)}
                  sections={this.getSections(products, productTypes)}
                />
              ) : (
                <View style={styles.messageContainer}>
                  <TextComponent
                    style={styles.messageText}
                    text={"Sorry! There are no products."}
                  />
                  <TextComponent
                    style={styles.messageText}
                    text={"Use the button below to add them."}
                  />
                </View>
              )}
              <ButtonComponent
                onPress={() =>
                  navigate("ProductTypes", {
                    addTitleText: "Select a product type"
                  })
                }
                style={styles.addButton}
              >
                <IconComponent
                  name="add"
                  size={40}
                  color="white"
                  style={styles.icon}
                />
              </ButtonComponent>
            </View>
          );
        }}
      </Query>
    );
  }
}

const styles = StyleSheet.create({
  header: {
    position: "absolute",
    backgroundColor: "transparent",
    zIndex: 100,
    top: 0,
    left: 0,
    right: 0
  },
  container: {
    backgroundColor: "#FFF5EE",
    paddingTop: 10,
    paddingLeft: 10,
    paddingRight: 10,
    flex: 1
  },
  categoryText: {
    fontSize: 16,
    paddingLeft: 3,
    paddingBottom: 8,
    color: "#442C2E"
  },
  card: {
    height: 47
  },
  cardText: {
    fontSize: 14,
    color: "#442C2E",
    alignSelf: "center"
  },
  messageContainer: {
    marginTop: "auto",
    marginBottom: "auto"
  },
  messageText: {
    fontSize: 16,
    fontWeight: "500",
    color: "#442C2E",
    textAlign: "center"
  },
  addButton: {
    alignSelf: "flex-end",
    position: "absolute",
    zIndex: 1,
    bottom: 28,
    right: 10,
    borderRadius: 62.1,
    width: 62.1,
    height: 62.1,
    backgroundColor: "#09A494"
  },
  icon: {
    alignSelf: "center",
    marginTop: "auto",
    marginBottom: "auto"
  }
});
