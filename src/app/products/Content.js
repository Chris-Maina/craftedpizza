import React, { Component } from "react";

import { 
  View,
  SectionList,
  ActivityIndicator,
} from "react-native";

import {
  ButtonComponent,
  IconComponent,
  TextComponent,
  ErrorComponent,
  ListItemComponent
} from "../common";
import styles from "./styles";

class Content extends Component {
  renderItem = item => {
    const { handleProductClick } = this.props;
    return (
      <ListItemComponent
        key={item.id}
        style={styles.card}
        onPress={() => handleProductClick(item)}
      >
        <TextComponent text={item.name} style={styles.cardText} />
        {item.size && (
          <TextComponent text={item.size} style={styles.cardText} />
        )}
        {item.weight && (
          <TextComponent text={item.weight} style={styles.cardText} />
        )}
        <TextComponent text={item.price} style={styles.cardText} />
      </ListItemComponent>
    );
  };

  /**
   * @name getSections
   * Helper method to generate sections from product types
   * @param {Array} - products
   * @param {Array} - productTypes
   * @returns {Array} - sections
   */
  getSections = (products, productTypes) => {
    const sections = [];
    for (type of productTypes) {
      const data = products.filter(
        item => item.product_type.title === type.title
      );
      if (!data.length) continue;
      sections.push({ title: type.title, data });
    }
    return sections;
  };

  render() {
    const { handleAddClick, loading, error, data, refetch } = this.props;
    const { products, productTypes } = data;

    if (loading) return <ActivityIndicator size="large" color="#EF4DB6" />;
    if (error)
      return (
        <ErrorComponent
          message="There was an error loading products"
          retry={() => refetch()}
        />
      );
    return (
      <View style={styles.container}>
        {products.length ? (
          <SectionList
            keyExtractor={item => item.id}
            renderSectionHeader={({ section }) => (
              <TextComponent text={section.title} style={styles.categoryText} />
            )}
            renderItem={({ item }) => this.renderItem(item)}
            sections={this.getSections(products, productTypes)}
          />
        ) : (
          <View style={styles.messageContainer}>
            <TextComponent
              style={styles.messageText}
              text={"Sorry! There are no products."}
            />
            <TextComponent
              style={styles.messageText}
              text={"Use the button below to add them."}
            />
          </View>
        )}
        <ButtonComponent
          onPress={handleAddClick}
          style={styles.addButton}
        >
          <IconComponent
            name="add"
            size={40}
            color="white"
            style={styles.icon}
          />
        </ButtonComponent>
      </View>
    );
  }
}

export default Content;
