import React, { Component } from "react";
import { Query } from "react-apollo";

import Content from "./Content";
import { GET_PRODUCTS } from "./duck";

class ProductContainer extends Component {
  static navigationOptions = {
    title: "Price list",
    headerStyle: {
      backgroundColor: "#FFF5EE",
      border: 0
    }
  };

  handleProductClick = item => {
    const {
      navigation: { navigate }
    } = this.props;
    navigate("SingleProduct", { item });
  };

  handleAddClick = () => {
    const {
      navigation: { navigate }
    } = this.props;
    navigate("ProductTypes", {
      addTitleText: "Select a product type"
    });
  };

  render() {
    return (
      <Query query={GET_PRODUCTS}>
        {({ loading, error, data, refetch }) => (
          <Content
            data={data}
            error={error}
            loading={loading}
            refetch={refetch}
            handleAddClick={this.handleAddClick}
            handleProductClick={this.handleProductClick}
          />
        )}
      </Query>
    );
  }
}

export default ProductContainer;
