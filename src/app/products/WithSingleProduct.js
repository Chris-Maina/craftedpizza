import React, { Component } from 'react';
import { Mutation } from 'react-apollo';

import { DELETE_PRODUCT, GET_PRODUCTS } from './duck';

class WithSingleProduct extends Component {

  /**
   * @name handleDeleteClick
   * @description handle product deletion
   */
  handleDeleteClick = (mutation, id) => {
    const { navigation: { navigate } } = this.props;
    mutation({ variables: { id, }});
    navigate('Prices');
  }

  handleEditClick = () => {
    const { item } = this.props.navigation.state.params;
    this.props.navigation.navigate('EditProduct', { item });
  }

  render() {
    const { item } = this.props.navigation.state.params;
    return(
      <Mutation
        mutation={DELETE_PRODUCT}
        update={
          (cache) => {
            const { products } = cache.readQuery({ query: GET_PRODUCTS });
            cache.writeQuery({
              query: GET_PRODUCTS,
              data: { products: products.filter(el => el.id !== item.id)}
            });
          }
        }
      >
        {
          deleteProduct => (
            this.props.render(
              item,
              () => this.handleDeleteClick(deleteProduct, item.id),
              this.handleEditClick
            )
          )
        }
      </Mutation>
    )
  }
}

export default WithSingleProduct;
