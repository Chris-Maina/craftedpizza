import React from 'react';
import { TextInput, StyleSheet } from 'react-native';

export const TextInputComponent = (props) => {
  const { placeholder, style, value, onChangeText, multiline, keyboardType } = props;
  return (
    <TextInput
      multiline={multiline}
      style={[styles.input, style]}
      placeholder={placeholder}
      value={value}
      keyboardType={keyboardType}
      onChangeText={onChangeText}
    />
  );
}

const styles = StyleSheet.create({
  input: {
    height: 40,
    padding: 10,
    backgroundColor: '#ffffff',
    borderRadius: 10,
    color: '#442C2E'
  },
});
