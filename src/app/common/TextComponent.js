import React from 'react';
import { Text } from 'react-native';

export const TextComponent = (props) => (
  <Text style={props.style}>{props.text}</Text>
);
