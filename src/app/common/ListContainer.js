import React from 'react';
import { View, StyleSheet } from 'react-native';

export const ListContainer = (props) => (
  <View style={[styles.container, props.style]}>{props.children}</View>
);

const styles = StyleSheet.create({
  container: {
    flex: 1, 
    paddingTop: 20,
    paddingLeft: 10,
    paddingRight: 10,
    backgroundColor: '#F9FBFD'
  }
})