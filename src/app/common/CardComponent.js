import React from "react";
import { TouchableOpacity, StyleSheet } from "react-native";

export const CardComponent = props => {
  const { style, children, onPress } = props;
  return (
    <TouchableOpacity style={[styles.container, style]} onPress={onPress}>
      {children}
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  container: {
    borderRadius: 2,
    borderWidth: 0.2,
    shadowOffset: { width: 2, height: 2 },
    shadowOpacity: 1
  }
});
