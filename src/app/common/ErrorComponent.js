import React from 'react';
import { View , StyleSheet } from 'react-native';
import { TextComponent } from './TextComponent';
import { IconComponent } from './IconComponent';
import { ButtonComponent } from './ButtonComponent';

export const ErrorComponent = (props) => {
  const { retry, message } = props;
  return (
    <View style={styles.container}>
      <IconComponent 
        name='error'
        size={50}
        style={styles.icon}
      />
      <TextComponent text={message} style={styles.message} />
      { retry && 
          <ButtonComponent style={styles.retryButton} onPress={() => (retry())}>
            <TextComponent text='Try again' style={styles.retryText} />
          </ButtonComponent>
      }
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#F9FBFD',
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  icon: {
    marginBottom: 10
  },
  message: {
    fontSize: 18,
    color: 'black',
    marginBottom: 10
  },
  retryButton: {
    paddingLeft: 'auto',
    paddingRight: 'auto',
    paddingTop: 5,
    paddingBottom: 5,
    borderWidth: 0
  },
  retryText: {
    fontSize: 18,
    color: '#0459f7'
  }
})
