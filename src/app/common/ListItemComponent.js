import React from 'react';
import { StyleSheet } from 'react-native';
import { CardComponent } from './CardComponent'

export const ListItemComponent = (props) => (
  <CardComponent style={[styles.card, props.style]} onPress={props.onPress}>
    {props.children}
  </CardComponent>
);

const styles = StyleSheet.create({
  card: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    shadowOpacity: 0.5,
    backgroundColor: 'white',
    marginBottom: 20
  },
});
