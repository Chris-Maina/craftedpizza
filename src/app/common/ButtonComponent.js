import React from 'react';
import { TouchableOpacity } from 'react-native';

export const ButtonComponent = (props) => (
  <TouchableOpacity
    style={props.style}
    onPress={props.onPress}
  >
    {props.children}
  </TouchableOpacity>
);
