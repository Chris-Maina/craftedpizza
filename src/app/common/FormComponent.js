import React from 'react';
import { StyleSheet, View } from 'react-native';
import {
  ListContainer,
  TextComponent,
  TextInputComponent,
  DropdownComponent,
  ButtonComponent,
  IconComponent
} from '../common';

export const FormComponent = (props) => {
    const { fields, iconName, handleSubmit, cancelText, cancelClick} = props;
    return (
      <ListContainer style={[ styles.container ]}>
        {fields.map((field, index) => {
          if (field.type == 'input') {
            return (
              <View style={styles.inputContainer} key={index}>
                <TextComponent style={styles.label} text={field.name} />
                <TextInputComponent
                  value={field.name}
                  onChangeText={field.cb}
                  placeholder={field.placeholder}
                  value={field.value}
                />
                <TextComponent
                  text={field.renderValidationError(field.name.toLowerCase())}
                  style={styles.errorMessage}
                />
              </View>
            );
          } else if (field.type == "dropdown") {
            return (
              <View style={styles.inputContainer} key={index}>
                <TextComponent style={styles.label} text={field.name} />
                <DropdownComponent
                  options={field.options}
                  title={field.value}
                  toggleItem={field.cb}
                />
                <TextComponent
                  text={field.renderValidationError(field.name.toLowerCase())}
                  style={styles.errorMessage}
                />
              </View>
            );
          }
        })}
        <View style={styles.buttonContainer}>
          <ButtonComponent style={styles.cancelButton} onPress={cancelClick}>
            <TextComponent
              text={cancelText}
              style={styles.cancelText}
              />
          </ButtonComponent>
          <ButtonComponent style={styles.button} onPress={handleSubmit}>
            <IconComponent
              name={iconName}
              size={40}
              color='white'
              style={styles.icon}
            />
          </ButtonComponent>
        </View>
      </ListContainer>
    );
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#FFF5EE'
  },
  label: {
    fontSize: 18,
    marginBottom: 5,
    color: '#442C2E',
  },
  inputContainer: {
    marginBottom: 20
  },
  errorMessage: {
    color: "red",
    textTransform: 'capitalize'
  },
  buttonContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between'
  },
  button: {
    borderRadius: 62.1,
    width: 62.1,
    height: 62.1,
    backgroundColor: '#09A494',
  },
  icon: {
    alignSelf: 'center',
    marginTop: 'auto',
    marginBottom: 'auto',
  },
  cancelButton: {
    borderRadius: 8,
    width: 115.6,
    height: 40,
    backgroundColor: '#FFFFFF',
    
  },
  cancelText: {
    alignSelf: 'center',
    marginTop: 'auto',
    marginBottom: 'auto',
    color: '#442C2E',
    fontSize: 18
  }
});
