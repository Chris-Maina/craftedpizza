import React from 'react';
import Icon from 'react-native-vector-icons/MaterialIcons'; 

export const IconComponent = (props) => (
  <Icon
    style={props.style}
    name={props.name}
    size={props.size}
    color={props.color}
  />
  );

