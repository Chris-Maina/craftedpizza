import React, { Component } from 'react';
import { StyleSheet, View, FlatList, Text, TouchableOpacity, TouchableHighlight } from 'react-native';

export class DropdownComponent extends Component{

  static getDerivedStateFromProps(nextProps){
    const { title } = nextProps;
    if(title) {
      if(title.name && title.size) {
        return { headerTitle: `${title.name}            ${title.price}              ${title.size}`}
      } else if (title.name && !title.size) {
        return { headerTitle: `${title.name}            ${title.price} ` };
      } else {
        return { headerTitle: title }
      }
    }
    return null;
  }

  constructor(props){
    super(props)
    this.state = {
      listOpen: false,
      headerTitle: this.props.title
    }
  }

  handleClickOutside = () =>{
    this.setState({
      listOpen: false
    })
  }

  toggleList = () => {
    this.setState(prevState => ({
      listOpen: !prevState.listOpen
    }));
  }

  onItemPress= (id, key, options) => {
    const { toggleItem } = this.props;
    toggleItem(id, key, options);
    this.toggleList();

  }
 
  render() {
    const { options } = this.props;
    const { listOpen, headerTitle } = this.state;
    return (
     <View style={styles.container}>
       <TouchableOpacity style={styles.headerContainer} onPress={this.toggleList}>
        <Text style={styles.headerTitle}>{headerTitle}</Text>
       </TouchableOpacity>
       { 
         listOpen &&
          <FlatList
            style={styles.listContainer}
            data={options}
            keyExtractor={item => (item.id.toString())}
            renderItem={({ item }) => {
              const { title, id, key } = item;
              return (
              <TouchableHighlight onPress={() => this.onItemPress(id, key, options)}>
                <View style={styles.textContainer} >
                  <Text style={[styles.item, styles.title]}>{title.name || title}</Text>
                  { title.size && <Text style={[styles.item, styles.subtitle]}>{title.size}</Text> }
                  { title.price && <Text style={[styles.item, styles.pricetitle]}>{title.price}</Text> }
                </View>
              </TouchableHighlight>
              );
            }
          }
          />
       }
     </View>
    );
  }
} 

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#ffffff',
    borderColor: '#ffffff',
    borderRadius: 10
  },
  headerContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    height: 40,
    padding: 10,
    backgroundColor: '#ffffff',
    borderRadius: 10
  },
  headerTitle: {
    color: '#442C2E'
  },
  listContainer: {
    backgroundColor: '#ffffff',
    paddingLeft: 10,
    paddingRight: 10,
    borderColor: '#ffffff'
  },
  textContainer: {
    flexDirection: 'row',
    justifyContent: 'space-evenly'
  },
  item: {
    paddingBottom: 10,
    paddingTop: 10,
    color: '#442C2E',
  },
  title: {
    marginRight: 'auto'
  },
  pricetitle: {
    marginLeft: 'auto',
  }
});
