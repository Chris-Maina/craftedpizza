import React, { Component } from "react";
import { Query } from "react-apollo";
import { View, ActivityIndicator } from "react-native";

import { GET_ORDER } from "../../duck";
import { capitalize } from "../../../../services/capitalize";
import { TextComponent, ErrorComponent, IconComponent } from "../../../common";

import styles from "./styles";

class SingleSalesContainer extends Component {
  static navigationOptions = {
    title: "Details",
    headerStyle: {
      ...styles.headerStyle,
      border: "none"
    }
  };

  renderSaleDetails = orderProduct => {
    return (
      <View style={styles.blockWrapper}>
        <TextComponent style={styles.blockTitle} text="Products" />
        {orderProduct.map(item => (
          <View style={styles.detailsContainer} key={item.id}>
            <TextComponent style={styles.text} text={item.quantity} />
            <TextComponent
              style={{ ...styles.text, marginLeft: 10 }}
              text={capitalize(item.product.name)}
            />
            <TextComponent
              style={{ ...styles.text, marginLeft: 10 }}
              text={item.product.price}
            />
            {item.product.size && (
              <TextComponent
                style={{ ...styles.text, marginLeft: 10 }}
                text={item.product.size}
              />
            )}
            <IconComponent
              name="delete"
              size={20}
              color="red"
              style={styles.deleteIcon}
            />
          </View>
        ))}
      </View>
    );
  };

  renderDeliveryDetails = delivery => {
    const { location, cost } = delivery;
    return (
      <View style={styles.blockWrapper}>
        <View style={styles.blockTitleWrapper}>
          <TextComponent style={styles.blockTitle} text="Delivery" />
          <IconComponent name="edit" size={20} color="#09A494" />
        </View>
        {location && (
          <TextComponent style={styles.text} text={`Location:  ${location}`} />
        )}
        {cost && (
          <TextComponent style={styles.text} text={`Cost: KES ${cost}`} />
        )}
      </View>
    );
  };

  renderCustomerDetails = customer => {
    const { name, phoneNumber } = customer;
    return (
      <View style={styles.blockWrapper}>
        <View style={styles.blockTitleWrapper}>
          <TextComponent style={styles.blockTitle} text="Customer" />
          <IconComponent name="edit" size={20} color="#09A494" />
        </View>
        {name && <TextComponent style={styles.text} text={`Name: ${name}`} />}
        {phoneNumber && (
          <TextComponent
            style={styles.text}
            text={`Phone number:  ${phoneNumber}`}
          />
        )}
      </View>
    );
  };

  render() {
    const {
      navigation: { state }
    } = this.props;
    const { order } = state.params;
    return (
      <Query query={GET_ORDER} variables={{ id: order.id }}>
        {({ loading, error, data, refetch }) => {
          if (loading)
            return <ActivityIndicator size="large" color="#EF4DB6" />;
          if (error)
            return (
              <ErrorComponent
                message="There was an error loading sale details."
                retry={() => refetch()}
              />
            );
          const {
            order: { orderProduct, delivery, customer }
          } = data;

          return (
            <View style={styles.container}>
              {orderProduct.length && this.renderSaleDetails(orderProduct)}
              {delivery && this.renderDeliveryDetails(delivery)}
              {customer && this.renderCustomerDetails(customer)}
            </View>
          );
        }}
      </Query>
    );
  }
}

export default SingleSalesContainer;
