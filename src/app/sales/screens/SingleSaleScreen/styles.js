const styles = {
  headerStyle: {
    backgroundColor: "#FFF5EE"
  },
  container: {
    backgroundColor: "#FFF5EE",
    alignItems: "center",
    paddingTop: 40,
    flex: 1
  },
  blockWrapper: {
    backgroundColor: "#fff",
    padding: 10,
    borderWidth: 0,
    borderRadius: 7,
    shadowColor: "#000",
    width: "90%",
    shadowOffset: { width: 0, height: 1 },
    shadowOpacity: 0.8,
    shadowRadius: 1,
    marginBottom: 40
  },
  blockTitleWrapper: {
    flexDirection: "row",
    justifyContent: "space-between"
  },
  blockTitle: {
    fontWeight: "700",
    fontSize: 17,
    marginBottom: 10,
    color: "#B9B9B9"
  },
  detailsContainer: {
    flexDirection: "row"
  },
  text: {
    fontSize: 14,
    marginBottom: 6
  },
  deleteIcon: {
    marginLeft: "auto"
  }
};

export default styles;
