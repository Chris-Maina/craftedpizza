import React, { Component } from "react";
import { View } from "react-native";
import Wizard from "react-native-wizard";
import { Button } from "react-native-paper";

import styles from './styles';
import { CustomerForm, DeliveryForm, SaleCaptureForm } from "../../components";

class AddSaleScreen extends Component {
  static navigationOptions = {
    title: "Add Sale",
    headerStyle: {
      ...styles.headerStyle,
      border: 0
    }
  };

  state = {
    name: "",
    phoneNumber: "",
    location: "",
    deliveryType: "",
    quantity: "",
    product: "",
    category: "",
    currentStep: 0,
    isLastStep: false,
    isFirstStep: false,
    productMenuVisible: false,
    categoryMenuVisible: false,
    deliveryMenuVisible: false
  };

  // wizard = React.createRef();

  onChangeText = (name, value) => {
    this.setState({ [name]: value });
  };

  handleOpenMenu = menuType => {
    this.setState({ [menuType]: true });
  };

  handleCloseMenu = menuType => {
    this.setState({ [menuType]: false });
  };

  setCurrentStep = step => {
    this.setState({ currentStep: step });
  }

  toggleBoolState = (state, bool) => {
    this.setState({ [state]: bool });
  }

  render() {
    const {
      name,
      phoneNumber,
      location,
      deliveryType,
      deliveryMenuVisible,
      product,
      category,
      quantity,
      productMenuVisible,
      categoryMenuVisible
    } = this.state;

    const stepList = [
      {
        content: (
          <CustomerForm
            name={name}
            phoneNumber={phoneNumber}
            onChangeText={this.onChangeText}
          />
        )
      },
      {
        content: (
          <DeliveryForm
            location={location}
            deliveryType={deliveryType}
            deliveryMenuVisible={deliveryMenuVisible}
            onChangeText={this.onChangeText}
            handleCloseMenu={this.handleCloseMenu}
            handleOpenMenu={this.handleOpenMenu}
          />
        )
      },
      {
        content: (
          <SaleCaptureForm
            product={product}
            category={category}
            quantity={quantity}
            products={[]}
            categories={[]}
            onChangeText={this.onChangeText}
            handleCloseMenu={this.handleCloseMenu}
            handleOpenMenu={this.handleOpenMenu}
            productMenuVisible={productMenuVisible}
            categoryMenuVisible={categoryMenuVisible}
          />
        )
      }
    ];
    return (
      <View>
        <Wizard
          steps={stepList}
          ref={e => this.wizard = e}
          isLastStep={val => this.toggleBoolState('isLastStep', val)}
          isFirstStep={val => this.toggleBoolState('isFirstStep', val)}
          currentStep={({ currentStep }) => {
            this.setCurrentStep(currentStep)
          }}
        />
        <Button mode="contained" onPress={() => console.log("Pressed")}>
          Next
        </Button>
      </View>
    );
  }
}

export default AddSaleScreen;
