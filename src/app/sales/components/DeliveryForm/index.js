import React from "react";
import { View, Text } from "react-native";
import { TextInput, Menu, Button } from "react-native-paper";

import { DELIVERY_TYPES } from "../../constants";

const DeliveryForm = props => {
  const {
    location,
    deliveryType,
    deliveryMenuVisible,
    onChangeText,
    handleCloseMenu,
    handleOpenMenu,
  } = props;

  const typeSelected = deliveryType || 'Type'
  return (
    <View>
      <Text>Delivery</Text>
      <Text>Enter details of the Delivery</Text>
      <View>
        <TextInput
          label="Location"
          value={location}
          onChangeText={value => onChangeText('location', value)}
        />
        <Menu
          onDismiss={() => handleCloseMenu("deliveryMenuVisible")}
          visible={deliveryMenuVisible}
          anchor={
            <Button onPress={() => handleOpenMenu("deliveryMenuVisible")}>
              {typeSelected}
            </Button>
          }
        >
          {DELIVERY_TYPES.map(type => (
            <Menu.Item key={type} onPress={() => onChangeText('deliveryType', type)}>
              {type}
            </Menu.Item>
          ))}
        </Menu>
      </View>
    </View>
  );
};

export default DeliveryForm;
