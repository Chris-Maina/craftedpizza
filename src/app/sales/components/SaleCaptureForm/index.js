import React from "react";
import { View, Text } from "react-native";
import { TextInput, Menu, Button } from "react-native-paper";

const SaleCaptureForm = props => {
  const {
    product,
    category,
    quantity,
    products,
    categories,
    productMenuVisible,
    categoryMenuVisible,
    onChangeText,
    handleCloseMenu,
    handleOpenMenu,
  } = props;

  const categoryMenuTitle = category || 'Category';
  const productMenuTitle = product || 'Product';
  return (
    <View>
      <Text>Sale</Text>
      {/* <Text>Enter details of the sale</Text> */}
      <View>
        <Menu
          visible={categoryMenuVisible}
          onDismiss={() => handleCloseMenu("categoryMenuVisible")}
          anchor={
            <Button onPress={() => handleOpenMenu("categoryMenuVisible")}>{categoryMenuTitle}</Button>
          }
        >
          {categories.map(category => (
            <Menu.Item onPress={() => onChangeText('category', category)}>
              {category}
            </Menu.Item>
          ))}
        </Menu>
        <Menu
          visible={productMenuVisible}
          onDismiss={() => handleCloseMenu("productMenuVisible")}
          anchor={
            <Button onPress={() => handleOpenMenu("productMenuVisible")}>{productMenuTitle}</Button>
          }
        >
          {products.map(product => (
            <Menu.Item onPress={() => onChangeText('product', product)}>
              {product}
            </Menu.Item>
          ))}
        </Menu>
        <TextInput
          label="Quantity"
          value={quantity}
          onChangeText={value => onChangeText('quantity', value)}
        />
      </View>
    </View>
  );
};

export default SaleCaptureForm;
