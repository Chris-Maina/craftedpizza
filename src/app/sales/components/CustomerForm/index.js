import React from 'react';
import { View, Text } from "react-native";
import { TextInput } from 'react-native-paper';

const CustomerForm = props => {
  const { name, phoneNumber, onChangeText } = props
  return (
    <View>
      <Text>Customer</Text>
      <Text>Enter details of the customer</Text>
      <View>
        <TextInput
          label="Name"
          value={name}
          onChangeText={value => onChangeText('name', value)}
        />
        <TextInput
          label="Phone number"
          value={phoneNumber}
          onChangeText={onChangeText}
          onChangeText={value => onChangeText('phoneNumber', value)}
        />
      </View>
    </View>
  )
}

export default CustomerForm;
