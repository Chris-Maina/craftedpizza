import { StyleSheet } from 'react-native';

const row = {
  flexDirection: 'row',
  justifyContent: 'space-between'
}
const styles = StyleSheet.create({
  card: {
    backgroundColor: '#fff',
    padding: 10,
    borderWidth: 0,
    borderRadius: 3,
    shadowColor: '#000',
    width: '90%',
    marginLeft: 'auto',
    marginRight: 'auto',
    shadowOffset: { width: 0, height: 0.5 },
    shadowOpacity: 0.4,
    shadowRadius: 1,
    marginBottom: 10,  
  },
  titleWrapper: {
   ...row,
   marginBottom: 8,
  },
  productDetails: {
    ...row,
    paddingTop: 5,
  },
  productWrapper: {
    ...row,
  },
  product: {
    marginLeft: 5,
    fontSize: 12,
    color: '#442C2E',
  },
  orderNumber: {
    fontWeight: "700",
    fontSize: 13,
  },
  date: {
    fontSize: 10,
    color: 'rgba(68,44,46, 0.5)'
  },
  totalWrapper: {
    alignSelf: 'center', 
  },
  total: {
    fontSize: 12,
    color: '#FF9900',
  }
})

export default styles;
