import React, { Component } from "react";
import { View, TouchableOpacity } from "react-native";

import styles from "./styles";
import { TextComponent } from "../../../common";
import { capitalize } from '../../../../services/capitalize';

class OrderComponent extends Component {
  /**
   * @name convertDate
   * converts date to local time
   * @param {String} - date
   */
  convertDate = utcdate => {
    const date = new Date(utcdate);
    const month = date.getMonth() + 1;
    const day = date.getDate();
    const year = date.getFullYear();

    return `${day}/${month}/${year}`;
  };

  getTotal = orderProduct => {
    let total = 0;
    orderProduct.forEach(order => {
      total = total + order.quantity * order.product.price;
    });
    return total;
  };

  render() {
    const {
      handleSaleOrderClick,
      order: { id, createdAt, orderProduct }
    } = this.props;
    return (
      <TouchableOpacity style={styles.card} onPress={() => handleSaleOrderClick(this.props.order)}>
        <View style={styles.titleWrapper}>
          <TextComponent style={styles.orderNumber} text={`Order #${id}`} />
          <TextComponent
            style={styles.date}
            text={this.convertDate(createdAt)}
          />
        </View>
        <View style={styles.productDetails}>
          <View>
            {orderProduct.length &&
              orderProduct.map(item => (
                <View style={styles.productWrapper} key={item.id}>
                  <TextComponent
                    style={styles.product}
                    text={capitalize(item.product.name)}
                  />
                  {item.product.size && (
                    <TextComponent
                      style={styles.product}
                      text={`${item.product.size}`}
                    />
                  )}
                  <TextComponent
                    style={styles.product}
                    text={`${item.quantity} `}
                  />
                </View>
              ))}
          </View>
          <View style={styles.totalWrapper}>
            <TextComponent
              style={styles.total}
              text={`Ksh ${this.getTotal(orderProduct)}`}
            />
          </View>
        </View>
      </TouchableOpacity>
    );
  }
}

export default OrderComponent;
