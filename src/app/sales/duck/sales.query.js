import gql from "graphql-tag";
import { PRODUCT_FRAGMENT } from '../../products/duck';
import { CUSTOMER_FRAGMENT } from '../../customers/duck';
import { DELIVERY_FRAGMENT } from '../../deliveries/duck';
import { PRODUCT_TYPE_FRAGMENT } from '../../productTypes/duck';

const ORDER_PRODUCTS_FRAGMENT = gql`
  fragment OrderProducts on OrderProduct {
    id
    quantity
    product {
      ...Product
      product_type {
        ...ProductType
      }
    }
  }
  ${PRODUCT_FRAGMENT}
  ${PRODUCT_TYPE_FRAGMENT}
`

export const GET_SALES_ORDERS = gql`
  query getOrders {
    orders {
      id
      createdAt
      orderProduct {
        ...OrderProducts
      }
    }
  }
  ${ORDER_PRODUCTS_FRAGMENT}
`;

export const GET_ORDER = gql`
  query getOrder($id: Int) {
    order(id: $id) {
      id
      createdAt
      customer {
        ...Customer
      }
      delivery {
        ...Delivery
      }
      orderProduct {
        ...OrderProducts
      }
    }
  }
  ${CUSTOMER_FRAGMENT}
  ${DELIVERY_FRAGMENT}
  ${ORDER_PRODUCTS_FRAGMENT}
`;
