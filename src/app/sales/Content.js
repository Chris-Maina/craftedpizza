import React from "react";
import { Query } from "react-apollo";
import { FAB } from "react-native-paper";
import { View, FlatList, ActivityIndicator, SafeAreaView } from "react-native";

import { Order } from "./components";
import styles from "./styles";
import { GET_SALES_ORDERS } from "./duck";
import { ErrorComponent } from "../common";

const Content = ({ handleSaleOrderClick, handleAddClick }) => {
  return (
    <Query query={GET_SALES_ORDERS}>
      {({ loading, error, data, refetch }) => {
        if (loading) return <ActivityIndicator size="large" color="#EF4DB6" />;
        if (error)
          return (
            <ErrorComponent
              message="There was an error loading data."
              retry={() => refetch()}
            />
          );
        const { orders } = data;
        return (
          <SafeAreaView>
            <FlatList
              data={orders}
              style={styles.container}
              contentContainerStyle={styles.scrollContent}
              keyExtractor={item => item.id}
              renderItem={({ item: order }) => (<Order order={order} handleSaleOrderClick={handleSaleOrderClick} />)}
            />
            <FAB
              icon="plus"
              color="#09A494"
              style={styles.fab}
              onPress={handleAddClick}
            />
          </SafeAreaView>
        );
      }}
    </Query>
  );
};

export default Content;
