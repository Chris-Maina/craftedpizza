import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  headerStyle: {
    backgroundColor: '#FFF5EE',
  },
  container: {
    flexGrow: 1,
    backgroundColor: '#FFF5EE',
  },
  scrollContent: {
    justifyContent: 'center',
    marginTop: 10,
  },
  fab: {
    position: 'absolute',
    margin: 16,
    right: 0,
    bottom: 0,
  },
});

export default styles;
