import React, { Component } from "react";

import styles from "./styles";
import Content from "./Content";

class SalesContainer extends Component {
  static navigationOptions = {
    title: "Sales",
    headerStyle: {
      ...styles.headerStyle,
      border: 0
    }
  };

  handleSaleOrderClick = order => {
    const {
      navigation: { navigate }
    } = this.props;

    navigate("SaleDetails", { order });
  };

  handleAddClick = () => {
    const {
      navigation: { navigate }
    } = this.props;

    navigate("AddSale");
  };

  render() {
    return (
      <Content
        handleAddClick={this.handleAddClick}
        handleSaleOrderClick={this.handleSaleOrderClick}
      />
    );
  }
}

export default SalesContainer;
