export const deleteItemInArray = (value, arr) => (
  arr.filter(item => (item.id != value.id))
);
