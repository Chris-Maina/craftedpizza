const validateFormFields = (formData, requiredFields) => {
  const errors = {};

  requiredFields.map((fieldName) => {
    if (typeof formData[fieldName] === 'number') return
    else if (typeof formData[fieldName] === 'object') {
      if(Object.keys(formData[fieldName]).length === 0) {
        errors[fieldName] = `This field is required`
      }
      return;
    }
    if(!formData[fieldName].trim().length) {
      errors[fieldName] = `This field is required`;
    } else if (fieldName === 'price' || fieldName === 'quantity' || fieldName === 'phoneNumber' || fieldName === 'cost') {
      if (isNaN(formData[fieldName]) || formData[fieldName] < 1) {
        errors[fieldName] = `This field  must be a positive integer`;
      }
    } else {
      delete errors.fieldName
    }
  });

  return errors;
}

export default validateFormFields;
