
const headers = {
  'Content-Type': 'application/json',
  Accept: 'application/json'
};

const checkStatus = (response) => {
  if (response.status >= 200 && response.status < 300 ) {
    return response;
  } else {
    const error = new Error(response.statusText);
    error.response = response;
    throw error;
  }
}

export const get = async (path) => {
  try {
    const response = await fetch(`http://localhost:3000/${path}`, { headers });
    const valid = checkStatus(response);
    const result = await valid.json();
    return result; 
  } catch (error) {
    throw error;
  }
}

export const post = async (path, data) => {
  try {
    const response = await fetch(`http://localhost:3000/${path}`, {
      headers,
      method: 'POST',
      body: JSON.stringify(data)
     });
    const valid = checkStatus(response);
    const result = await valid.json();
    return result; 
  } catch (error) {
    throw error;
  }
}

export const patch = async (path, data) => {
  try {
    const response = await fetch(`http://localhost:3000/${path}`, {
      headers,
      method: 'PATCH',
      body: JSON.stringify(data)
    });
    const valid = checkStatus(response);
    const result = await valid.json();
    return result; 
  } catch (error) {
    throw error;
  }
}

export const deleteRequest = async (path) => {
  try {
    const response = await fetch(`http://localhost:3000/${path}`, {
      headers,
      method: 'DELETE'
    });
    const valid = checkStatus(response);
    const result = await valid.json();
    return result;
  } catch (error) {
    throw error;
  }
}
