const editItemInArray = (value, arr) => (
  arr.map(item => (item.id == value.id ? value : item))
);

export default editItemInArray;
