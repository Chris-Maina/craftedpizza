/**
 * @name createField
 * Helps to create form fields
 * @param  {String} name - field label
 * @param  {String} type - field type
 * @param  {String} placeholder - field placeholder
 * @param  {String} value - field value
 * @param  {Array} option - dropdown options
 * @param  {func} cb - handler for change/toggle events in the case of dropdown
 * @param  {func} renderValidationError - error handler
 */
const createField = (...args) => {
  const [ name, type, placeholder, value, options, cb, renderValidationError ] = args;
  return{
  name,
  type,
  placeholder,
  value,
  options,
  cb,
  renderValidationError
}
};

export default createField;