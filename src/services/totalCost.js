/**
   * @name totalCost
   * @description returns the total cost of ordered items
   * @param {Array} - orders
   * @param {number} - delivery cost
   * @param {Array} - products
   */
  totalCost = (orders, deliveryCost, products) => {
    let result;
    result = orders.map(order => {
      const { quantity, productId } = order;
      const product = productId ? products.find(prod => prod.id === productId) : order.product;
      let total = 0;
      if (deliveryCost) {
        total += parseInt(deliveryCost);
      }
      return total + parseInt(quantity) * parseInt(product.price);
    });
    return result.reduce((acc, curr) => acc + curr);
}

export default totalCost;