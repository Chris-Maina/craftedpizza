/** @format */
import React from "react";
import { AppRegistry } from "react-native";

import App from "./src/App";
import ApolloClient from "apollo-boost";
import { ApolloProvider } from "react-apollo";
import { name as CraftedPizza } from "./app.json";
import { Provider as PaperProvider } from "react-native-paper";

const client = new ApolloClient({
  uri: "http://localhost:4000/graphql"
});

const Main = () => (
  <ApolloProvider client={client}>
    <PaperProvider>
      <App />
    </PaperProvider>
  </ApolloProvider>
);
AppRegistry.registerComponent(CraftedPizza, () => Main);
